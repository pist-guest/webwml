#use wml::debian::cdimage title="Hämta Debian-usb-/dvd-/cd-avbildningar via BitTorrent" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

#use wml::debian::translation-check translation="67bd189030c9c649cfb9f6581aafc637c6af3b8e"

<p>
<a href="https://sv.wikipedia.org/wiki/Bittorrent">BitTorrent</a> är ett
icke-hierarkiskt filhämntningssystem optimerat för stora mängder hämtar.
Det belastar vårt nätverk minimalt, då BitTorrentklienter sänder små delar av
filer till andra medan de hämtar och därmed sprider belastningen över nätverket,
vilket gör det möjligt att hämta filerna riktigt snabbt.
</p>

<div class="tip">
<p>
Den <strong>första</strong> usb/cd:n/dvd:n innehåller alla filer som behövs för
att installera ett vanligt Debiansystem.
<br />
</p>
</div>

<p>
Du behöver en BitTorrentklient för att hämta Debian-usb/dvd/cd:arna på det här sättet.
I Debiandistributionen finns verktygen
<a href="https://packages.debian.org/aria2">aria2</a>,
<a href="https://packages.debian.org/transmission">transmission</a> och
<a href="https://packages.debian.org/ktorrent">KTorrent</a>.
Stöd för andra operativsystem (som Windows och macOS) finns i
<a href="https://www.qbittorrent.org/download">qBittorrent</a> och
<a href="https://www.bittorrent.com/download">BitTorrent</a>.
</p>


<h3>Officiella torrenter för den <q>stabila</q> utgåvan</h3>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>

<p>Se till att läsa dokumentationen innan du installerar.
<strong>Om du bara tänker läsa ett dokument</strong> innan du installerar,
se vår
<a href="$(HOME)/releases/stable/amd64/apa">installationshjälp</a>, en snabb
genomgång av installationsprocessen.
Dessutom finns följande nyttiga dokumentation:
</p>

<ul>
 <li>
  <a href="$(HOME)/releases/stable/installmanual">Installationsguide</a>,
  de detaljerade installationsinstruktionerna.
 </li>
 <li>
  <a href="https://wiki.debian.org/DebianInstaller">Dokumentation för
  Debian-installer</a>, bland annat vanliga frågor med svar.
 </li>
 <li>
  <a href="$(HOME)/releases/stable/debian-installer/#errata">Errata för
  Debian-installer</a>, en förteckning över kända problem i
  installationsprogrammet.
 </li>
</ul>

# <h3>Officiella torrenter för <q>uttestnings</q>-utgåvan</h3>
#
# <ul>
#
#   <li><strong>CD</strong>:<br />
#   <full-cd-torrent>
#   </li>
#
#   <li><strong>DVD</strong>:<br />
#   <full-dvd-torrent>
#   </li>
#
# </ul>

<p>
Om du har möjlighet ber vi dig låta din klient fortsätta köra när hämtningen är
färdig så att du hjälper andra att hämta avbildningarna snabbare!
</p>
