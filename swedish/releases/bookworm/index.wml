#use wml::debian::template title="Versionsfakta för Debian &ldquo;Bookworm&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="77c9da4860132027cb6546223ff0e9b84071a5b7"

<if-stable-release release="bookworm">

<p>Debian <current_release_bookworm> släpptes
<a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "Debian 12.0 släpptes ursprungligen <:=spokendate('2023-06-10'):>."
/>
Utgåvan inkluderade många stora
förändringar, vilka beskrivs i vårt
our <a href="$(HOME)/News/2023/20230610">pressmeddelande</a> och
<a href="releasenotes">versionsfakta</a>.</p>

# <p><strong>Debian 12 har ersatts av
# <a href="../trixie/">Debian 13 (<q>Trixie</q>)</a>.
# </strong></p>

### This paragraph is orientative, please review before publishing!
# <p><srong>Bookworm stöds av långtidsstöd (Long Term Support - LTS) fram
# till slutet på 30 juni 2028. LTS begränsas till i386, amd64, armel, armhf och arm64.
# Inga andra arkitekturer stöd längre i Bookworm.
# För ytterligare information, vänligen se <a
# href="https://wiki.debian.org/LTS">LTS-avdelningen i Debian-wikin</a>.
# </strong></p>

<p>
Debian 12:s livscykel innefattar fem år: de ursprungliga tre åren med
fullständigt Debiansstöd, fram till <:=spokendate('2026-06-10'):>, och
två år med Långtidsstsöd (Long Term Support, LTS), till
<:=spokendate('2028-06-30'):>. Uppstättningen arkitekturer med stöd minskas
under LTS-perioden. För ytterligare information, vänligen se webbsidan
för <a href="$(HOME)/security/">säkerhetsinformation</a> samt
<a  href="https://wiki.debian.org/LTS">LTS-avdelningen av Debianwikin</a>.
</p>


<p>För att få tag på och installera Debian, se vår sida med
<a href="debian-installer/">installationsinformation</a> samt
<a href="installmanual">installationsguiden</a>. För att uppgradera från en
tidigare Debianutgåva, se informationen i
<a href="releasenotes">versionsfakta</a>.</p>

### Activate the following when LTS period starts.
#<p>Arkitekturer som stöd under perioden för långtidsstöd:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Datorarkitekturer som stöds i den ursprungliga utgåvan av Bookworm:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Tvärt emot våra önskemål finns det en del problem i denna utgåva, även om den
kallas för <em>stabil</em>. Vi har sammanställt
<a href="errata">en lista över de största kända problemen</a>, och du kan alltid
<a href="../reportingbugs">rapportera andra problem</a> till oss.</p>

<p>Sist, men inte minst, har vi en lista över <a href="credits">folk som skall
ha tack</a> för att ha möjliggjort denna version.</p>
</if-stable-release>


