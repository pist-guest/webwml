<define-tag pagetitle>Uppdaterad Debian 10; 10.13 utgiven</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c"

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin trettonde (och sista) uppdatering till dess
gamla stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Efter denna punktutgåva kommer inte Debians säkerhetsgrupp och utgåvegrupp
att producera uppdateringar till Debian 10. Användare som vill fortsätta få
säkerhetsstöd bör uppdatera till Debian 11, eller se <url "https://wiki.debian.org/LTS">
för detaljer om undergruppen av arkitekturer och paket som stöds av projektet
för långtidsstöd (Long Term Support).</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den gamla stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction adminer "Fix open redirect issue, cross-site scripting issues [CVE-2020-35572 CVE-2021-29625]; elasticsearch: Do not print response if HTTP code is not 200 [CVE-2021-21311]; provide a compiled version and configuration files">
<correction apache2 "Fix denial of service issue [CVE-2022-22719], HTTP request smuggling issue [CVE-2022-22720], integer overflow issue [CVE-2022-22721], out-of-bounds write issue [CVE-2022-23943], HTTP request smuggling issue [CVE-2022-26377], out-of-bounds read issues [CVE-2022-28614 CVE-2022-28615], denial of service issue [CVE-2022-29404], out-of-bounds read issue [CVE-2022-30556], possible IP-based authentication bypass issue [CVE-2022-31813]">
<correction base-files "Update for the 10.13 point release">
<correction clamav "New upstream stable release; security fixes [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction commons-daemon "Fix JVM detection">
<correction composer "Fix code injection vulnerability [CVE-2022-24828]; update GitHub token pattern; use Authorization header instead of deprecated access_token query parameter">
<correction debian-installer "Rebuild against buster-proposed-updates; increase Linux ABI to 4.19.0-21">
<correction debian-installer-netboot-images "Rebuild against buster-proposed-updates; increase Linux ABI to 4.19.0-21">
<correction debian-security-support "Update security status of various packages">
<correction debootstrap "Ensure non-merged-usr chroots can continue to be created for older releases and buildd chroots">
<correction distro-info-data "Add Ubuntu 22.04 LTS, Jammy Jellyfish and Ubuntu 22.10, Kinetic Kudu">
<correction dropbear "Fix possible username enumeration issue [CVE-2019-12953]">
<correction eboard "Fix segfault on engine selection">
<correction esorex "Fix testsuite failures on armhf and ppc64el caused by incorrect libffi usage">
<correction evemu "Fix build failure with recent kernel versions">
<correction feature-check "Fix some version comparisons">
<correction flac "Fix out-of-bounds write issue [CVE-2021-0561]">
<correction foxtrotgps "Fix build failure with newer imagemagick versions">
<correction freeradius "Fix side-channel leak where 1 in 2048 handshakes fail [CVE-2019-13456], denial of service issue due to multithreaded BN_CTX access [CVE-2019-17185], crash due to non-thread safe memory allocation">
<correction freetype "Fix buffer overflow issue [CVE-2022-27404]; fix crashes [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Fix buffer overflow issues [CVE-2022-25308 CVE-2022-25309]; fix crash [CVE-2022-25310]">
<correction ftgl "Don't try to convert PNG to EPS for latex, as our imagemagick has EPS disabled for security reasons">
<correction gif2apng "Fix heap-based buffer overflows [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction gnucash "Fix build failure with recent tzdata">
<correction gnutls28 "Fix test suite when combined with OpenSSL 1.1.1e or newer">
<correction golang-github-docker-go-connections "Skip tests that use expired certificates">
<correction golang-github-pkg-term "Fix building on newer 4.19 kernels">
<correction golang-github-russellhaering-goxmldsig "Fix NULL pointer dereference issue [CVE-2020-7711]">
<correction grub-efi-amd64-signed "New upstream release">
<correction grub-efi-arm64-signed "New upstream release">
<correction grub-efi-ia32-signed "New upstream release">
<correction grub2 "New upstream release">
<correction htmldoc "Fix infinite loop [CVE-2022-24191], integer overflow issues [CVE-2022-27114] and heap buffer overflow issue [CVE-2022-28085]">
<correction iptables-netflow "Fix DKMS build failure regression caused by Linux upstream changes in the 4.19.191 kernel">
<correction isync "Fix buffer overflow issues [CVE-2021-3657]">
<correction kannel "Fix build failure by disabling generation of Postscript documentation">
<correction krb5 "Use SHA256 as Pkinit CMS Digest">
<correction libapache2-mod-auth-openidc "Improve validation of the post-logout URL parameter on logout [CVE-2019-14857]">
<correction libdatetime-timezone-perl "Update included data">
<correction libhttp-cookiejar-perl "Fix build failure by increasing the expiry date of a test cookie">
<correction libnet-freedb-perl "Change the default host from the defunct freedb.freedb.org to gnudb.gnudb.org">
<correction libnet-ssleay-perl "Fix test failures with OpenSSL 1.1.1n">
<correction librose-db-object-perl "Fix test failure after 6/6/2020">
<correction libvirt-php "Fix segmentation fault in libvirt_node_get_cpu_stats">
<correction llvm-toolchain-13 "New source package to support building of newer firefox-esr and thunderbird versions">
<correction minidlna "Validate HTTP requests to protect against DNS rebinding attacks [CVE-2022-26505]">
<correction mokutil "New upstream version, to allow for SBAT management">
<correction mutt "Fix uudecode buffer overflow [CVE-2022-1328]">
<correction node-ejs "Sanitize options and new objects [CVE-2022-29078]">
<correction node-end-of-stream "Work around test bug">
<correction node-minimist "Fix prototype pollution issue [CVE-2021-44906]">
<correction node-node-forge "Fix signature verification issues [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-require-from-string "Fix a test in conjunction with nodejs &gt;= 10.16">
<correction nvidia-graphics-drivers "New upstream release">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream release; fix out-of-bound write issues [CVE-2022-28181 CVE-2022-28185]; security fixes [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction octavia "Fix client certificate checks [CVE-2019-17134]; correctly detect that the agent is running on Debian; fix template that generates vrrp check script; add additional runtime dependencies; ship additional configuration directly in the agent package">
<correction orca "Fix use with WebKitGTK 2.36">
<correction pacemaker "Update relationship versions to fix upgrades from stretch LTS">
<correction pglogical "Fix build failure">
<correction php-guzzlehttp-psr7 "Fix improper header parsing [CVE-2022-24775]">
<correction postfix "New upstream stable release; do not override user set default_transport; if-up.d: do not error out if postfix can't send mail yet; fix duplicate bounce_notice_recipient entries in postconf output">
<correction postgresql-common "pg_virtualenv: Write temporary password file before chowning the file">
<correction postsrsd "Fix potential denial of service issue when Postfix sends certain long data fields such as multiple concatenated email addresses [CVE-2021-35525]">
<correction procmail "Fix NULL pointer dereference">
<correction publicsuffix "Update included data">
<correction python-keystoneauth1 "Update tests to fix build failure">
<correction python-scrapy "Don't send authentication data with all requests [CVE-2021-41125]; don't expose cookies cross-domain when redirecting [CVE-2022-0577]">
<correction python-udatetime "Properly link against libm library">
<correction qtbase-opensource-src "Fix setTabOrder for compound widgets; add an expansion limit for XML entities [CVE-2015-9541]">
<correction ruby-activeldap "Add missing dependency on ruby-builder">
<correction ruby-hiredis "Skip some unreliable tests in order to fix build failure">
<correction ruby-http-parser.rb "Fix build failure when using http-parser containing the fix for CVE-2019-15605">
<correction ruby-riddle "Allow use of <q>LOAD DATA LOCAL INFILE</q>">
<correction sctk "Use <q>pdftoppm</q> instead of <q>convert</q> to convert PDF to JPEG as the latter fails with the changed security policy of ImageMagick">
<correction twisted "Fix incorrect URI and HTTP method validation issue [CVE-2019-12387], incorrect certificate validation in XMPP support [CVE-2019-12855], HTTP/2 denial of service issues, HTTP request smuggling issues [CVE-2020-10108 CVE-2020-10109 CVE-2022-24801], information disclosure issue when following cross-domain redirects [CVE-2022-21712], denial of service issue during SSH handshake [CVE-2022-21716]">
<correction tzdata "Update timezone data for Iran, Chile and Palestine; update leap andra list">
<correction ublock-origin "New upstream stable release">
<correction unrar-nonfree "Fix directory traversal issue [CVE-2022-30333]">
<correction wireshark "Fix remote code execution issue [CVE-2021-22191], denial of service issues [CVE-2021-4181 CVE-2021-4184 CVE-2021-4185 CVE-2022-0581 CVE-2022-0582 CVE-2022-0583 CVE-2022-0585 CVE-2022-0586]">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den gamla stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2021 4836 openvswitch>
<dsa 2021 4852 openvswitch>
<dsa 2021 4906 chromium>
<dsa 2021 4911 chromium>
<dsa 2021 4917 chromium>
<dsa 2021 4981 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5077 librecad>
<dsa 2022 5080 snapd>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5108 tiff>
<dsa 2022 5109 faad2>
<dsa 2022 5111 zlib>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5126 ffmpeg>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5135 postgresql-11>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5144 condor>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5167 firejail>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5173 linux-latest>
<dsa 2022 5173 linux-signed-amd64>
<dsa 2022 5173 linux-signed-arm64>
<dsa 2022 5173 linux-signed-i386>
<dsa 2022 5173 linux>
<dsa 2022 5174 gnupg2>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5185 mat2>
<dsa 2022 5186 djangorestframework>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction elog "Icke underhållen; säkerhetsproblem">
<correction libnet-amazon-perl "Beroende på bottaget API">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
gamla stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella gamla stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Föreslagna uppdateringar till den gamla stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Information om den gamla stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


