msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr ""

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr ""

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr ""

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr ""

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr ""

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr ""

#: ../../english/template/debian/legal_tags.wml:27
msgid "See the <a href=\"./\">license information</a> page for an overview of the Debian License Summaries (DLS)."
msgstr ""

