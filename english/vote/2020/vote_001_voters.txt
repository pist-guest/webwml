-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1           93sam	Steve McIntyre
    2             abe	Axel Beckert
    3        abhijith	Abhijith PA
    4           aboll	Andreas Boll
    5           adamm	Adam Majer
    6       adrianorg	Adriano Rafael Gomes
    7            adsb	Adam D. Barratt
    8             aeb	Andreas E. Bombe
    9             agi	Alberto Gonzalez Iniesta
   10           alexm	Alex Muntada
   11            alfs	Stefan Alfredsson
   12        ametzler	Andreas Metzler
   13             ana	Ana Beatriz Guerrero López
   14         anarcat	Antoine Beaupré
   15            anbe	Andreas Beckmann
   16            andi	Andreas B. Mundt
   17        andrewsh	Andrej Shadura
   18           angel	Angel Abad
   19          anibal	Anibal Monsalve Salazar
   20            ardo	Ardo van Rangelrooij
   21          arturo	Arturo Borrero González
   22           asias	Asias He
   23         aurel32	Aurelien Jarno
   24           aviau	Alexandre Viau
   25       awoodland	Alan Woodland
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26              az	Alexander Zangerl
   27        azekulic	Alen Zekulic
   28     balasankarc	Balasankar Chelamattathu
   29        ballombe	Bill Allombert
   30             bas	Bas Zoetekouw
   31           bayle	Christian Bayle
   32          bbaren	Benjamin Barenblat
   33           bdale	Bdale Garbee
   34            benh	Ben Hutchings
   35            beuc	Sylvain Beucler
   36         bgoglin	Brice Goglin
   37          bgupta	Brian Gupta
   38         bigeasy	Sebastian Andrzej Siewior
   39           blade	Eduard Bloch
   40           bluca	Luca Boccassi
   41           bmarc	Bertrand Marc
   42           bootc	Chris Boot
   43         bottoms	Maitland Bottoms
   44          boutil	Cédric Boutillier
   45         bremner	David Bremner
   46         broonie	Mark Brown
   47            bunk	Adrian Bunk
   48         bureado	Jose Parrella
   49            bzed	Bernd Zeimetz
   50        calculus	Jerome Georges Benoit
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51          carnil	Salvatore Bonaccorso
   52             ckk	Christian Kastner
   53           clint	Clint Adams
   54        codehelp	Neil Williams
   55            cord	Cord Beermann
   56         coucouf	Aurélien Couderc
   57          csmall	Craig Small
   58             cts	Christian T. Steigies
   59           cwryu	Changwoo Ryu
   60             dai	Daisuke Higuchi
   61          daniel	Daniel Baumann
   62           dapal	David Paleino
   63            dato	Dato Simó
   64       debalance	Philipp Huebner
   65          dkogan	Dima Kogan
   66       dktrkranz	Luca Falavigna
   67          dlange	Daniel Lange
   68           dlehn	David I. Lehn
   69             dmn	Damyan Ivanov
   70             dod	Dominique Dumont
   71         dogsleg	Lev Lamberov
   72             don	Don Armstrong
   73         donkult	David Kalnischkies
   74       dottedmag	Mikhail Gusarov
   75        dsilvers	Daniel Silverstone
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76          ebourg	Emmanuel Bourg
   77          eevans	Eric Evans
   78        ehashman	Elana Hashman
   79          elbrus	Paul Mathijs Gevers
   80          enrico	Enrico Zini
   81        eriberto	Joao Eriberto Mota Filho
   82            eric	Eric Dorland
   83           eriks	Erik Schanze
   84             evo	Davide Puricelli
   85           fabbe	Fabian Fagerholm
   86          fabian	Fabian Greffrath
   87             faw	Felipe Augusto van de Wiel
   88          fgeyer	Felix Geyer
   89             flo	Florian Lohoff
   90         florian	Florian Ernst
   91         fpeters	Frederic Peters
   92        francois	Francois Marier
   93        fsateler	Felipe Sateler
   94           fuddl	Bruno Kleinert
   95              fw	Florian Weimer
   96         gaudenz	Gaudenz Steinlin
   97         genannt	Jonas Genannt
   98        georgesk	Georges Khaznadar
   99           ghedo	Alessandro Ghedini
  100             gio	Giovanni Mascellani
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101         giovani	Giovani Augusto Ferreira
  102           gladk	Anton Gladky
  103        glaubitz	John Paul Adrian Glaubitz
  104          glondu	Stéphane Glondu
  105          gregoa	Gregor Herrmann
  106            gspr	Gard Spreemann
  107         guilhem	Guilhem Moulin
  108         guillem	Guillem Jover
  109           gwolf	Gunnar Wolf
  110        hartmans	Sam Hartman
  111         helmutg	Helmut Grohne
  112         hertzog	Raphaël Hertzog
  113      hlieberman	Harlan Lieberman-Berg
  114          holger	Holger Levsen
  115      hvhaugwitz	Hannes von Haugwitz
  116            ianw	Ian Wienand
  117             ijc	Ian James Campbell
  118       intrigeri	Intrigeri
  119        ishikawa	Ishikawa Mutsumi
  120           ivodd	Ivo De Decker
  121        iwamatsu	Nobuhiro Iwamatsu
  122             jan	Jan Niehusmann
  123           jandd	Jan Dittberner
  124          jathan	Jonathan Bustillos
  125         jbfavre	Jean Baptiste Favre
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126             jcc	Jonathan Cristopher Carter
  127          jelmer	Jelmer Vernooij
  128              jh	Jörgen Hägg
  129           jimmy	Jimmy Kaplowitz
  130             jjr	Jeffrey Ratcliffe
  131          jlines	John Lines
  132           joerg	Joerg Jaspert
  133           jordi	Jordi Mallach
  134           josch	Johannes Schauer
  135           josue	Josué Ortega
  136       jpmengual	Jean-Philippe MENGUAL
  137          jpuydt	Julien Puydt
  138        jredrejo	José L. Redrejo Rodríguez
  139          jrtc27	Jessica Clarke
  140              js	Jonas Smedegaard
  141        jspricke	Jochen Sprickerhof
  142          kartik	Kartik Mistry
  143          keithp	Keith Packard
  144        kilobyte	Adam Borowski
  145       kitterman	Scott Kitterman
  146            knok	Takatsugu Nokubi
  147           kobla	Ondřej Kobližek
  148           krala	Antonin Kral
  149      kritzefitz	Sven Bartscher
  150            kula	Marcin Kulisz
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151           lamby	Chris Lamb
  152           laney	Iain Lane
  153         larjona	Laura Arjona Reina
  154        lavamind	Jerome Charaoui
  155        lawrencc	Christopher Lawrence
  156      lazyfrosch	Markus Frosch
  157     leatherface	Julien Patriarca
  158         lenharo	Daniel Lenharo de Souza
  159             leo	Carsten Leonhardt
  160        lfilipoz	Luca Filipozzi
  161        lightsey	John Lightsey
  162         lingnau	Anselm Lingnau
  163          lkajan	Laszlo Kajan
  164          lohner	Nils Boeffel
  165         lopippo	Filippo Rusconi
  166           lucab	Luca Bruno
  167           lucas	Lucas Nussbaum
  168           lumin	Mo Zhou
  169         madduck	Martin F. Krafft
  170            mafm	Manuel A. Fernandez Montecelo
  171             mak	Matthias Klumpp
  172            maks	Maximilian Attems
  173           mattb	Matthew Brown
  174     matthieucan	Matthieu Caneill
  175        matthijs	Matthijs Möhlmann
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  176          mattia	Mattia Rizzolo
  177         mbehrle	Mathias Behrle
  178           mbuck	Martin Buck
  179              md	Marco d'Itri
  180            mejo	Jonas Meurer
  181          merkys	Andrius Merkys
  182          meskes	Michael Meskes
  183             mfv	Matteo F. Vescovi
  184             mhy	Mark Hymers
  185           micah	Micah Anderson
  186           micha	Micha Lenk
  187          midget	Dario Minnucci
  188             mih	Michael Hanke
  189            mika	Michael Prokop
  190           milan	Milan Kupcevic
  191        mjeanson	Michael Jeanson
  192           mones	Ricardo Mones Lastra
  193           moray	Moray Allan
  194           mpitt	Martin Pitt
  195             mrd	Matthew Danish
  196              mt	Michael Tautschnig
  197     mtecknology	Michael Lustfield
  198        mtmiller	Mike Miller
  199           murat	Murat Demirten
  200        mwhudson	Michael Hudson-Doyle
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  201            myon	Christoph Berg
  202           neilm	Neil McGovern
  203           nickm	Nick Morrott
  204         nicolas	Nicolas Boulenguez
  205           noahm	Noah Meyerhans
  206          nodens	Clément Hermann
  207         nomeata	Joachim Breitner
  208         noodles	Jonathan McDowell
  209           ntyni	Niko Tyni
  210            odyx	Didier Raboud
  211           ohura	Makoto OHURA
  212           olasd	Nicolas Dandrimont
  213         olebole	Ole Streicher
  214            olek	Olek Wojnar
  215            olly	Olly Betts
  216          ondrej	Ondrej Sury
  217           onovy	Ondřej Nový
  218            pabs	Paul Wise
  219    paddatrapper	Kyle Robbertze
  220             pdm	Milan Zamazal
  221             peb	Pierre-Elliott Bécue
  222            phls	Paulo Henrique de Lima Santana
  223            piem	Paul Brossier
  224            pini	Gilles Filippini
  225            pino	Pino Toscano
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  226           piotr	Piotr Ożarowski
  227             pjb	Phil Brooke
  228           pkern	Philipp Kern
  229          plessy	Charles Plessy
  230       pmatthaei	Patrick Matthäi
  231           pollo	Louis-Philippe Véronneau
  232        porridge	Marcin Owsiany
  233         praveen	Praveen Arimbrathodiyil
  234        preining	Norbert Preining
  235        pronovic	Kenneth J. Pronovici
  236        pvaneynd	Peter Van Eynde
  237             qjb	Jay Berkenbilt
  238          rafael	Rafael Laboissiere
  239             ras	Russell Stuart
  240         rbalint	Balint Reczey
  241             rcw	Robert Woodcock
  242         reichel	Joachim Reichel
  243      rfrancoise	Romain Francoise
  244            roam	Peter Pentchev
  245         roberto	Roberto C. Sanchez
  246          roland	Roland Rosenfeld
  247        rousseau	Ludovic Rousseau
  248           rover	Roberto Lumbreras
  249             rra	Russ Allbery
  250     rvandegrift	Ross Vandegrift
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  251       samueloph	Samuel Henrique
  252        santiago	Santiago Ruano Rincón
  253           satta	Sascha Steinbiss
  254          sbadia	Sebastien Badia
  255        schultmc	Michael C. Schultheiss
  256         seamlik	Kai-Chung Yan
  257             seb	Sebastien Delafond
  258       sebastien	Sébastien Villemot
  259         serpent	Tomasz Rybak
  260              sf	Stefan Fritsch
  261          sfrost	Stephen Frost
  262             sjr	Simon Richter
  263           skitt	Stephen Kitt
  264            smcv	Simon McVittie
  265             smr	Steven Michael Robbins
  266           smurf	Matthias Urlichs
  267       spwhitton	Sean Whitton
  268       sramacher	Sebastian Ramacher
  269            srud	Sruthi Chandran
  270          ssgelm	Stephen Gelman
  271      stapelberg	Michael Stapelberg
  272        stefanor	Stefano Rivera
  273           steve	Steve Kostecke
  274         stevenc	Steven Chamberlain
  275       sthibault	Samuel Thibault
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  276             sto	Sergio Talens-Oliag
  277            sune	Sune Vuorela
  278           sur5r	Jakob Haufe
  279            tach	Taku Yasui
  280          taffit	David Prévot
  281          takaki	Takaki Taniguchi
  282           taowa	Taowa Munene-Tardif
  283             tbm	Martin Michlmayr
  284         tehnick	Boris Pek
  285        terceiro	Antonio Terceiro
  286          tfheen	Tollef Fog Heen
  287              tg	Thorsten Glaser
  288         thansen	Tobias Hansen
  289           thijs	Thijs Kinkhorst
  290           tiago	Tiago Bortoletto Vaz
  291          tianon	Tianon Gravi
  292          tijuca	Carsten Schoenert
  293           tille	Andreas Tille
  294            timo	Timo Jyrinki
  295            tiwe	Timo Weingärtner
  296        tjhukkan	Teemu Hukkanen
  297        tmancill	Tony Mancill
  298            tobi	Tobias Frost
  299           toddy	Tobias Quathamer
  300            toni	Toni Mueller
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  301         treinen	Ralf Treinen
  302           troyh	Troy Heber
  303        tvainika	Tommi Vainikainen
  304        tvincent	Thomas Vincent
  305           tytso	Theodore Y. Ts'o
  306         tzafrir	Tzafrir Cohen
  307            ucko	Aaron M. Ucko
  308          uhoreg	Hubert Chan
  309        ukleinek	Uwe Kleine-König
  310          ulrike	Ulrike Uhlig
  311       ultrotter	Guido Trotter
  312        umlaeute	IOhannes m zmölnig
  313           urbec	Judit Foglszinger
  314         utkarsh	Utkarsh Gupta
  315         vagrant	Vagrant Cascadian
  316        valhalla	Elena Grandi
  317         vanicat	Remi Vanicat
  318         vasudev	Vasudev Sathish Kamath
  319          vcheng	Vincent Cheng
  320        vdanjean	Vincent Danjean
  321           viiru	Arto Jantunen
  322         vlegout	Vincent Legout
  323          vorlon	Steve Langasek
  324          vvidic	Valentin Vidic
  325          wagner	Hanno Wagner
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  326           waldi	Bastian Blank
  327          weasel	Peter Palfrader
  328        weinholt	Göran Weinholt
  329           wferi	Ferenc Wágner
  330          wouter	Wouter Verhelst
  331             xam	Max Vozeler
  332          xluthi	Xavier Lüthi
  333            yadd	Xavier Guimard
  334            zack	Stefano Zacchiroli
  335            zeha	Christian Hofstaedtler
  336            zhsj	Shengjing Zhu
  337            zigo	Thomas Goirand
  338          zlatan	Zlatan Todoric
  339         zordhak	Alban Vidal
