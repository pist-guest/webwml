<define-tag pagetitle>General Resolution: Statement about the EU Legislation "Cyber Resilience Act and Product Liability Directive"</define-tag>
<define-tag status>F</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Discussion Period:</th>
	<td>2023-11-12</td>
	<td>2023-12-03</td>
      </tr>
      <tr>
	<th>Voting period:</th>
	<td>Saturday 2023-12-09 00:00:00 UTC</td>
	<td>Friday 2023-12-22 23:59:59 UTC</td>
      </tr>
    </table>

    <vproposera />
    <p>Santiago Ruano Rincón [<email santiago@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2023/11/msg00000.html'>text of original proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2023/11/msg00117.html'>text of amended proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2023/11/msg00125.html'>text of final amended proposal</a>]
    </p>
    <vsecondsa />
    <ol>
        <li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00001.html'>mail</a>]</li>
        <li>Mattia Rizzolo [<email mattia@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00002.html'>mail</a>]</li>
        <li>Lisandro Damián Nicanor Pérez Meyer [<email lisandro@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00003.html'>mail</a>]</li>
        <li>Nicolas Dandrimont [<email olasd@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00004.html'>mail</a>]</li>
        <li>Simon Quigley [<email tsimonq2@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00016.html'>mail</a>]</li>
        <li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00019.html'>mail</a>]</li>
        <li>Emmanuel Arias  [<email eamanu@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00062.html'>mail</a>]</li>
    </ol>
    <vtexta />
<h3>Choice 1: CRA and PLD proposals include regulations detrimental to FOSS</h3>

    <h4>Debian Public Statement about the EU Cyber Resilience Act and the
    Product Liability Directive</h4>

    <p>The European Union is currently preparing a regulation "on horizontal
    cybersecurity requirements for products with digital elements" known as
    the Cyber Resilience Act (CRA). It is currently in the final "trilogue"
    phase of the legislative process. The act includes a set of essential
    cybersecurity and vulnerability handling requirements for manufacturers.
    It will require products to be accompanied by information and
    instructions to the user. Manufacturers will need to perform risk
    assessments and produce technical documentation and, for critical
    components, have third-party audits conducted. Discovered security
    issues will have to be reported to European authorities within 24 hours
    (1). The CRA will be followed up by the Product Liability Directive
    (PLD) which will introduce compulsory liability for software.</p>

    <p>While a lot of these regulations seem reasonable, the Debian project
    believes that there are grave problems for Free Software projects
    attached to them. Therefore, the Debian project issues the following
    statement:</p>

    <ol>
    <li>   Free Software has always been a gift, freely given to society, to
    take and to use as seen fit, for whatever purpose. Free Software has
    proven to be an asset in our digital age and the proposed EU Cyber
    Resilience Act is going to be detrimental to it.
        <ol class="a" style="list-style-type: lower-alpha;">
        <li>  As the Debian Social Contract states, our goal is "make the best
    system we can, so that free works will be widely distributed and used."
    Imposing requirements such as those proposed in the act makes it legally
    perilous for others to redistribute our work and endangers our commitment
    to "provide an integrated system of high-quality materials with no legal
    restrictions that would prevent such uses of the system". (2)</li>

        <li>  Knowing whether software is commercial or not isn't feasible,
    neither in Debian nor in most free software projects - we don't track
    people's employment status or history, nor do we check who finances
    upstream projects (the original projects that we integrate in our
    operating system).</li>

        <li>  If upstream projects stop making available their code
    for fear of being in the
    scope of CRA and its financial consequences, system security will
    actually get worse rather than better.</li>

        <li>  Having to get legal advice before giving a gift to society
    will discourage many developers, especially those without a company or
    other organisation supporting them.</li></ol></li>

    <li>  Debian is well known for its security track record through practices
    of responsible disclosure and coordination with upstream developers and
    other Free Software projects. We aim to live up to the commitment made
    in the Debian Social Contract: "We will not hide problems." (3)

        <ol class="a" style="list-style-type: lower-alpha;">
        <li>  The Free Software community has developed a fine-tuned,
    tried-and-tested system of responsible disclosure in case of security
    issues which will be overturned by the mandatory reporting to European
    authorities within 24 hours (Art. 11 CRA).</li>

        <li>  Debian spends a lot of volunteering time on security issues,
    provides quick security updates and works closely together with upstream
    projects and in coordination with other vendors. To protect its users,
    Debian regularly participates in limited embargos to coordinate fixes to
    security issues so that all other major Linux distributions can also have
    a complete fix when the vulnerability is disclosed.</li>

        <li>  Security issue tracking and remediation is intentionally
    decentralized and distributed. The reporting of security issues to
    ENISA and the intended propagation to other authorities and national
    administrations would collect all software vulnerabilities in one place.
    This greatly increases the risk of leaking information about vulnerabilities
    to threat actors, representing a threat for all the users around the
    world, including European citizens.</li>

        <li>  Activists use Debian (e.g. through derivatives such as Tails),
    among other reasons, to protect themselves from authoritarian
    governments; handing threat actors exploits they can use for oppression
    is against what Debian stands for.</li>

        <li>  Developers and companies will downplay security issues because
    a "security" issue now comes with legal implications. Less clarity on
    what is truly a security issue will hurt users by leaving them vulnerable.</li></ol></li>

    <li>  While proprietary software is developed behind closed doors, Free
    Software development is done in the open, transparent for everyone. To
    retain parity with proprietary software the open development process needs
    to be entirely exempt from CRA requirements, just as the development of
    software in private is. A "making available on the market" can only be
    considered after development is finished and the software is released.</li>

    <li>  Even if only "commercial activities" are in the scope of CRA, the
    Free Software community - and as a consequence, everybody - will lose a
    lot of small projects. CRA will force many small enterprises and most
    probably all self employed developers out of business because they
    simply cannot fulfill the requirements imposed by CRA. Debian and other
    Linux distributions depend on their work. If accepted as it is,
    CRA will undermine not only an established community but also a
    thriving market. CRA needs an exemption for small businesses and, at the
    very least, solo-entrepreneurs.</li>
    </ol>

    <hrline />

    <p>Sources:</p>

    <p>(1)<br />
    <a href='https://www.europarl.europa.eu/legislative-train/theme-a-europe-fit-for-the-digital-age/file-european-cyber-resilience-act'>CRA proposals and links</a><br />
    <a href='https://www.europarl.europa.eu/legislative-train/theme-a-europe-fit-for-the-digital-age/file-new-product-liability-directive'>PLD proposals and links</a></p>

    <p>(2) <a href='https://www.debian.org/social_contract'>Debian Social Contract No. 2, 3 and 4</a></p>

    <vproposerb />
    <p>Luca Boccassi [<email bluca@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2023/11/msg00065.html'>text of proposal</a>]
    </p>
    <vsecondsb />
    <ol>
        <li>Aigars Mahinovs [<email aigarius@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00073.html'>mail</a>]</li>
        <li>Chris Hofstaedtler [<email zeha@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00075.html'>mail</a>]</li>
        <li>Lucas Nussbaum [<email lucas@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00076.html'>mail</a>]</li>
        <li>Michael Biebl [<email biebl@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00077.html'>mail</a>]</li>
        <li>Russ Allbery [<email rra@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00123.html'>mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00134.html'>mail</a>]</li>
    </ol>
    <vtextb />
<h3>Choice 2: CRA and PLD proposals should only apply to commercial ventures</h3>

    <h4>Debian Public Statement about the EU Cyber Resilience Act and the
    Product Liability Directive</h4>

    <p>The European Union is currently preparing a regulation "on horizontal
    cybersecurity requirements for products with digital elements" known as
    the Cyber Resilience Act (CRA). It's currently in the final "trilogue"
    phase of the legislative process. The act includes a set of essential
    cybersecurity and vulnerability handling requirements for manufacturers.
    It will require products to be accompanied by information and
    instructions to the user. Manufacturers will need to perform risk
    assessments and produce technical documentation and for critical
    components, have third-party audits conducted. Security issues under
    active exploitation will have to be reported to European authorities
    within 24 hours (1). The CRA will be followed up by an update to the
    existing Product Liability Directive (PLD) which, among other things,
    will introduce the requirement for products on the market using software
    to be able to receive updates to address security vulnerabilities.</p>

    <p>Given the current state of the electronics and computing devices market,
    constellated with too many irresponsible vendors not taking enough
    precautions to ensure and maintain the security of their products,
    resulting in grave issues such as the plague of ransomware (that, among
    other things, has often caused public services to be severely hampered or
    shut down entirely, across the European Union and beyond, to the
    detriment of its citizens), the Debian project welcomes this initiative
    and supports its spirit and intent.</p>

    <p>The Debian project believes Free and Open Source Software Projects to be
    very well positioned to respond to modern challenges around security and
    accountability that these regulations aim to improve for products
    commercialized on the Single Market. Debian is well known for its
    security track record through practices of responsible disclosure and
    coordination with upstream developers and other Free and Open Source
    Software projects. The project aims to live up to the commitment made in
    the Debian Social Contract: "We will not hide problems." (2)</p>

    <p>The Debian project welcomes the attempt of the legislators to ensure
    that the development of Free and Open Source Software is not negatively
    affected by these regulations, as clearly expressed by the European
    Commission in response to stakeholders' requests (1) and as stated in
    Recital 10 of the preamble to the CRA:</p>

<pre>
      'In order not to hamper innovation or research, free and open-source
      software developed or supplied outside the course of a commercial
      activity should not be covered by this Regulation.'
</pre>

    <p>The Debian project however notes that not enough emphasis has been
    employed in all parts of these regulations to clearly exonerate Free
    and Open Source Software developers and maintainers from being subject
    to the same liabilities as commercial vendors, which has caused
    uncertainty and worry among such stakeholders.</p>

    <p>Therefore, the Debian project asks the legislators to enhance the
    text of these regulations to clarify beyond any reasonable doubt that
    Free and Open Source Software developers and contributors are not going
    to be treated as commercial vendors in the exercise of their duties when
    merely developing and publishing Free and Open Source Software, with
    special emphasis on clarifying grey areas, such as donations,
    contributions from commercial companies and developing Free and Open
    Source Software that may be later commercialised by a commercial vendor.
    It is fundamental for the interests of the European Union itself that
    Free and Open Source Software development can continue to thrive and
    produce high quality software components, applications and operating
    systems, and this can only happen if Free and Open Source Software
    developers and contributors can continue to work on these projects as
    they have been doing before these new regulations, especially but not
    exclusively in the context of nonprofit organizations, without being
    encumbered by legal requirements that are only appropriate for
    commercial companies and enterprises.</p>

    <hrline />

    <p>Sources:</p>

    <p>(1)<br />
    <a href='https://www.europarl.europa.eu/legislative-train/theme-a-europe-fit-for-the-digital-age/file-proposal-for-cybersecurity-regulation'>CRA proposals and links</a><br />
    <a href='https://www.europarl.europa.eu/legislative-train/theme-a-europe-fit-for-the-digital-age/file-new-product-liability-directive'>PLD proposals and links</a><br />
    <a href='https://www.europarl.europa.eu/doceo/document/E-9-2023-002473-ASW_EN.html'>Response from the European Commission to a question from the European Parliament on FOSS awareness</a></p>

    <p>(2) <a href='https://www.debian.org/social_contract'>Debian Social Contract No. 2, 3 and 4</a></p>

    <vproposerc />
    <p>Bart Martens [<email bartm@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2023/11/msg00102.html'>text of proposal</a>]
    </p>
    <vsecondsc />
    <ol>
        <li>ChangZhuo Chen (陳昌倬) [<email czchen@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00105.html'>mail</a>]</li>
        <li>Paul Wise [<email pabs@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00108.html'>mail</a>]</li>
        <li>Simon Richter [<email sjr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00110.html'>mail</a>]</li>
        <li>Laura Arjona Reina [<email larjona@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00113.html'>mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2023/11/msg00135.html'>mail</a>]</li>
    </ol>

    <vtextc />
<h3>Choice 3: The EU should not overrule DFSG 6 and FOSS licenses</h3>

<h4>Debian Public Statement about the EU Cyber Resilience Act (CRA) and the
Product Liability Directive (PLD)</h4>

<p>The CRA includes requirements for manufacturers of software, followed
up by the PLD with compulsory liability for software. The Debian
project has concerns on the impact on Free and Open-Source Software
(FOSS).</p>

<p>The CRA makes the use of FOSS in commercial context more difficult.
This goes against the philosophy of the Debian project. The Debian Free
Software Guidelines (DFSG) include "6. No Discrimination Against Fields
of Endeavor - The license must not restrict anyone from making use of
the program in a specific field of endeavor." A significant part of the
success of FOSS is its use in commercial context. It should remain
possible for anyone to produce, publish and use FOSS, without making it
harder for commercial entities or for any group of FOSS users.</p>

<p>The compulsory liability as meant in the PLD overrules the usual
liability disclaimers in FOSS licenses. This makes sharing FOSS with
the public more legally risky. The compulsory liability makes sense for
closed-source software, where the users fully depend on the
manufacturers. With FOSS the users have the option of helping
themselves with the source code, and/or hiring any consultant on the
market. The usual liability disclaimers in FOSS licenses should remain
valid without the risk of being overruled by the PLD.</p>

<p>The Debian project asks the EU to not draw a line between commercial
and non-commercial use of FOSS. Such line should instead be between
closed-source software and FOSS. FOSS should be entirely exempt from
the CRA and the PLD.</p>

    <vquorum />

     <p>
        With the current list of <a href="vote_002_quorum.log">voting
          developers</a>, we have:
     </p>
    <pre>
#include 'vote_002_quorum.txt'
    </pre>
#include 'vote_002_quorum.src'


    <vstatistics />
    <p>
	For this GR, like always,
#                <a href="https://vote.debian.org/~secretary/gr_cra_pld/">statistics</a>
               <a href="suppl_002_stats">statistics</a>
             will be gathered about ballots received and
             acknowledgements sent periodically during the voting
             period.
               Additionally, the list of <a
             href="vote_002_voters.txt">voters</a> will be
             recorded. Also, the <a href="vote_002_tally.txt">tally
             sheet</a> will also be made available to be viewed.
         </p>

    <vmajorityreq />
    <p>
      All proposal need a simple majority.
    </p>
#include 'vote_002_majority.src'

    <voutcome />
#include 'vote_002_results.src'

    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Debian Project Secretary</a>
      </address>

