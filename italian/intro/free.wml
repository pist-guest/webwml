#use wml::debian::template title="Cosa significa free?" MAINPAGE="true"
#use wml::debian::translation-check translation="8d8a7b1eda812274f83d0370486ddb709d4f7d54" maintainer="Ceppo"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Free come in...?</a></li>
    <li><a href="#licenses">Licenze software</a></li>
    <li><a href="#choose">Come scegliere una licenza?</a></li>
  </ul>
</div>

<aside>
  <p>
    <span class="fas fa-caret-right fa-3x"></span>
    Nel febbraio 1998, un gruppo di persone si mosse per sostituire il termine
    <a href="https://www.gnu.org/philosophy/free-sw">Free Software</a> con <a
    href="https://opensource.org/osd">Software Open
    Source</a>. Questo dibattito sulla terminologia riflette le differenze
    filosofiche sottostanti, ma i requisiti concreti, così come altre cose
    discusse in questo sito, sono sostanzialmente le stesse per il Free
    Software e il Software Open Source.
  </p>
</aside>

<h2><a id="freesoftware">Free come in...?</a></h2>

<p>
Molte persone che si avvicinano per la prima volta a questo tema sono confuse
dalla parola «free». Questa non è utilizzata nella maniera che si aspettano:
per loro «free» significa «gratuito». Se si cerca il significato in un
dizionario di Inglese, esso elenca quasi venti significati diversi di «free» e
solo uno di questi è «gratis». Quindi, quando parliamo di Free Software, ci
riferiamo alla libertà, non al pagare.
</p>

<p>
Il software descritto come «free», ma solo nel senso che non è necessario
pagarlo, di solito non è affatto «free». Potrebbe essere vietato distribuirlo e
quasi sicuramente non è consentito migliorarlo. Il software distribuito
gratuitamente è di solito un'arma in una campagna di marketing per promuovere
un prodotto correlato o per portare fuori dal mercato un piccolo concorrente.
Non c'è garanzia che rimanga gratuito.
</p>

<p>
Per l'inesperto, un software o è free o non lo è. Ma la vita reale è molto più
complicata di così. Per capire cosa intende la gente quando parla di software
free dobbiamo fare una piccola deviazione ed entrare nel mondo delle licenze
software.
</p>

<h2><a id="licenses">Licenze software</a></h2>

<p>
Il copyright è una maniera di proteggere i diritti dei creatori di certi tipi
di opera. Nella maggior parte delle nazioni, il software scritto ex novo è
automaticamente coperto dal copyright. Una licenza è lo strumento attraverso il
quale l'autore permette ad altri di usare la sua creazione (in questo caso il
software) in modo accettabile per lui. Spetta all'autore includere una licenza
che dichiara come il software può essere usato.
</p>

<p style="text-align:center">
  <button type="button">
    <span class="fas fa-book-open fa-2x"></span>
    <a href="https://www.copyright.gov/">
      Più informazioni sul copyright (in Inglese)
    </a>
  </button>
</p>

<p>
Certamente circostanze diverse richiedono licenze diverse. Le compagnie di
software cercano di proteggere le loro risorse, quindi spesso distribuiscono
solo codice compilato che non può essere letto dagli umani. Inoltre pongono
molte restrizioni all'uso del software. Al contrario, gli autori di free
software cercano perlopiù regole diverse, talvolta anche una combinazione dei
punti seguenti:
</p>

<ul>
  <li>Non è permesso usare il loro codice in software proprietario. Dato che
  distribuiscono il loro software affinché chiunque possa usarlo, non vogliono
  vederlo rubato da altri. In questo caso l'uso del codice è considerato
  vincolato al rispetto di un  patto: si può usare finché si gioca secondo le
  stesse regole.</li>
  <li>Bisogna tutelare la paternità. Le persone sono orgogliose del loro lavoro
  e non vogliono che altri rimuovano il loro nome dai riconoscimenti o che
  addirittura sostengano di aver scritto personalmente il software.</li>
  <li>Il codice sorgente deve essere distribuito. Uno dei problemi principali
  del software proprietario è che non si possono correggere bug né
  personalizzare il programma, perché il codice sorgente non è disponibile.
  Inoltre, un produttore potrebbe decidere di non supportare più l'hardware
  dell'utente. La distribuzione del codice sorgente, obbligatoria sotto la
  maggior parte delle licenze libere, protegge gli utenti permettendo loro di
  personalizzare il software e adattarlo alle proprie necessità.</li>
  <li>Qualunque opera (detta «opera derivata») che contenga parti dell'opera
  degli autori deve usare la stessa licenza.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>
Tre delle licenze più usate per il free software sono la <a
href="https://www.gnu.org/copyleft/gpl.html">GNU General Public License
(GPL)</a>, la <a
href="https://opensource.org/blog/license/artistic-2-0">Artistic
License</a> e la <a href="https://opensource.org/blog/license/bsd-3-clause">BSD
Style License</a>.
</aside>

<h2><a id="choose">Come scegliere una licenza?</a></h2>

<p>
A volte le persone scrivono le proprie licenze, il che può essere problematico,
quindi è disapprovato dalla comunità del free software. Troppo spesso la
formulazione è ambigua o le persone creano condizioni che entrano in conflitto
l'una con l'altra. Scrivere una licenza che regga in tribunale è ancora più
difficile. Fortunatamente sono disponibili numerose licenze Open Source tra cui
scegliere. Esse hanno in comune le cose seguenti:
</p>

<ul>
  <li>Gli utenti possono installare il software su quante macchine
  vogliono.</li>
  <li>Il software può essere usato contemporaneamente da qualsiasi numero di
  persone.</li>
  <li>Gli utenti possono fare del software il numero di copie che desiderano o
  quello di cui hanno bisogno e anche dare queste copie ad altri utenti
  (distribuzione libera o aperta).</li>
  <li>Non ci sono limitazioni alla modifica del software (eccetto mantenere
  certi riconoscimenti).</li>
  <li>Gli utenti possono non solo distribuire il software, ma anche
  venderlo.</li>
</ul>

<p>
Soprattutto quest'ultimo punto, che consente alle persone di vendere il software,
sembra contrario all'idea stessa del free software, ma in realtà è uno dei suoi
punti di forza. Dato che la licenza permette la libera ridistribuzione, una
volta che una persona ottiene una copia può distribuire il software a sua
volta. Può anche provare a venderla.
</p>

<p>
Sebbene il free software non sia totalmente libero da vincoli, esso dà agli
utenti la flessibilità per fare ciò di cui hanno bisogno per raggiungere il
proprio obiettivo. Allo stesso tempo, i diritti dell'autore sono tutelati:
questa è libertà. Il progetto Debian e i suoi membri sono convinti sostenitori
del free software. Abbiamo stilato le <a
href="../social_contract#guidelines">Linee Guida Debian per il Software Free
(DFSG)</a> per avere una definizione ragionevole di ciò che secondo noi
costituisce il free software. Solo il software che si rispetta le DFSG è
consentito nella distribuzione principale di Debian.
</p>
