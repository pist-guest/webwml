#use wml::debian::translation-check translation="c9366063243a1d6e2294fc8ccd4eae1bf6812255" maintainer="Giuseppe Sacco"

<ul>
<li><a href="https://security-tracker.debian.org/">Debian Security Tracker</a>
fonte principale per tutte le informazioni riguardo la securezza, con possibilità di ricerca</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">elenco JSON</a>
  contiene le descrizioni CVE, i nomi dei pacchetti, i numeri Debian dei bug, le versioni
  dei pacchetti con le correzioni, non ci sono i DSA
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">elenco DSA</a>
  contiene i dati dei DSA, i numeri dei relativi CVE, le versioni dei pacchetti con le correzioni
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">elenco DLA</a>
  contiene i dati dei DLA, i numeri dei relativi CVE, le versioni dei pacchetti con le correzioni
</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
annunci DSA</a> (Debian Security Advisories)</li>
<li><a href="https://lists.debian.org/debian-lts-announce/">
annunci DLA</a> (Debian Security Advisories of Debian LTS)</li>

<li> <a class="rss_logo" style="float: none;" href="$(HOME)/security/dsa">RSS</a> dei DSA o
<a class="rss_logo" style="float: none;" href="$(HOME)/security/dsa-long">RSS</a> versione
estesa che include il testo dell'annuncio</li>

<li> <a class="rss_logo" style="float: none;" href="$(HOME)/lts/security/dla">RSS</a> dei DLA o
<a class="rss_logo" style="float: none;" href="$(HOME)/lts/security/dla-long">RSS</a> versione 
estesa che include il testo dell'annuncio</li>

<li><a href="oval">Oval files</a></li>

<li>Ricerca dei DSA (il maiuscolo è importante)<br>
esempio <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>Ricerca dei DLA ( -1 è importante)<br>
esempio <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>Ricerca dei CVE<br>
esempio <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>
