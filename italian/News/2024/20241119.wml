<define-tag pagetitle>Il Progetto Debian piange la scomparsa di Jérémy Bobbio (Lunar)</define-tag>
<define-tag release_date>2024-11-19</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="645e78e5a0801f8f9bd112dbe4eb9b199dcbf331" maintainer="Luca Monducci"

<p>Il Progetto Debian con grande tristezza annuncia la scomparsa di Jérémy
Bobbio (Lunar), avvenuta venerdì 8 novembre 2024.</p>

<p>Lunar era molto conosciuto e rispettato in diverse aree delle comunità
Linux e delle comunità del software Free/Libre Open Source (FLOSS); era
altrettanto noto come convinto sostenitore delle libertà individuali e
collettive.</p>

<p>Lunar era impegnato e sempre  presente nella progettazione e nella
diffusione di <a href="https://reproducible-builds.org/">Reproducible
Builds</a>. Per molte persone, la loro introduzione a questo sistema di
controllo e bilanciamento nel software è avvenuta direttamente attraverso
le sue parole, in numerose conferenze, interventi e mailing list.</p>

<p>Lunar è stato anche un valoroso sostenitore e un ambasciatore deciso
nelle questioni riguardanti la sorveglianza, la censura e la protezione
della privacy, il che lo ha portato a lavorare e sviluppare relazioni e
software all'interno del <a href="https://torproject.org/">Progetto Tor</a>.
Lunar è stato determinante nell'aiutare la rete a sviluppare strumenti,
istituire connessioni e affrontare questioni legali per operatori e
amministratori.</p>

<p>La scomparsa di Lunar lascia un vuoto in molte comunità. Andremo avanti,
continueremo a espandere le aree che gli stavano a cuore e proseguiremo il
suo lavoro in sua memoria. Lunar, ci mancherai!</p>

<p>Messaggi e ricordi sono stati espressi in tutto il mondo alla notizia
della sua scomparsa; qui condividiamo alcune di queste riflessioni:</p>

<blockquote>
<p>Il suo lavoro su Reproducible Builds, insieme ad altre persone in questa
lista, è stato trasformazionale per Debian e, oserei dire, avanti rispetto
ai tempi per un settore che sta lentamente scoprendo l'enorme minaccia
rappresentata dagli attacchi alla supply chain (il più recente a XZ).</p>
</blockquote>
<p>&mdash;&nbsp;Faidon Liambotis</p>

<blockquote>
<p>Ho sempre osservato e prestato attenzione al suo livello di dettaglio su
ciò che altri avrebbero considerato banale, ma attraverso la sua comprensione
e spiegazione quei dettagli minori si trasformavano in una comprensione
critica. Un vero mentore.</p>
</blockquote>
<p>&mdash;&nbsp;Donald Norwood</p>

<blockquote>
<p>Lunar era una persona incredibile, e mi manca profondamente, nonostante lo
vedessi solo di tanto in tanto. Creativo, riflessivo, intelligente e gentile,
ma non indiscriminatamente. Pungente quando necessario, ma anche disposto a
tendere una mano o mostrare grazia quando serviva. Mi mancheranno il suo
spirito tagliente, le sue osservazioni incisive e le sue provocazioni a essere
una persona migliore.</p>
</blockquote>

<blockquote>
<p>Le persone che lavorano sul software libero e su progetti affini formano
una comunità migliore grazie alla presenza di Lunar tra noi. Continuo a
cercare di vivere secondo gli standard che Lunar ha fissato.</p>
</blockquote>
<p>&mdash;&nbsp;dkg</p>

<blockquote>
<p>Ricordo in particolare una delle ultime conversazioni che ho avuto con
Lunar, qualche mese fa, quando mi disse quanto fosse orgoglioso non solo di
aver avviato Nos Oignons e contribuito all'innesco dei Reproducible Builds,
ma soprattutto del fatto che entrambe le iniziative stessero prosperando
senza dipendere da lui. Probabilmente pensava a un mondo futuro senza di
lui, ma anche al grande impatto che il suo attivismo aveva avuto sul mondo
passato e presente.</p>
</blockquote>
<p>&mdash;&nbsp;Stefano Zacchiroli</p>

<blockquote>
<p>Ricordo diversi momenti, ma stranamente quello che mi viene in mente più
spesso è quando l'ho incontrato nel campus a Portland durante il DebConf14,
quando era appena tornato con una grande scatola di ciambelle vegane da
condividere. Era così felice di quel momento! Su una nota più seria, penso
che il suo impatto su Tor, Debian e Reproducible Builds sia difficile da
sovrastimare, e c'è anche un ampio campo di <q>questioni offline</q> a
cui teneva profondamente, eppure è sempre rimasto umile e disponibile. Sono
incredibilmente grato per tutto il suo lavoro e per aver potuto condividere
del lavoro e dei bei momenti insieme.</p>
</blockquote>
<p>&mdash;&nbsp;Holger Levsen</p>

<h2>Su Debian</h2>
<p>Il progetto Debian è un'associazione di sviluppatori di software libero
che volontariamente offrono il loro tempo e il loro lavoro per produrre il
sistema operativo libero Debian.</p>

<h2>Come contattarci</h2>
<p>Per maggiori informazioni visitare le pagine web su
<a href="$(HOME)/">https://www.debian.org/</a> o mandare un email a
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
