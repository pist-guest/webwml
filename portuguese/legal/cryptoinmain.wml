#use wml::debian::template title="Explorando software criptográfico no repositório main do Debian" BARETITLE=true
#use wml::debian::translation-check translation="ba01cfdc529712e3626bdf15fd37d39e94126794"

# Nota! Isto é basicamente um rascunho do texto dos(as) advogados(as), e
# _não_ deve ser modificado nos erros ortográficos e semelhantes. Alterações de
# formatação podem ser feitas (eu acho) -- Joy

# Nota do time de tradução pt-br (Note from the pt-br translation team)
<p>
Nota de tradução&nbsp;: esta tradução é fornecida apenas para informação.
Se você estiver preocupado(a) com as questões levantadas por este documento,
consulte <a href="../legal/cryptoinmain.en.html">a versão em inglês</a>.
</p>

<table width="100%" summary="mail headers">
<colgroup span="2">
<col width="10%">
<col>
</colgroup>
<tr><td>Para:</td>
	<td><a href="https://www.spi-inc.org/">Software in the Public Interest</a>, <a href="https://www.debian.org/">Projeto Debian</a></td></tr>
<tr><td>De:</td>
	<td>Roszel C. Thomsen II, Partner, <a href="http://www.t-b.com/">Thomsen &amp; Burke LLP</a></td></tr>
<tr><td>Data:</td>
	<td>31 de julho de 2001</td></tr>
<tr><td>Re:</td>
	<td>Explorando software criptográfico no repositório main do Debian</td></tr>
</table>

<p>Obrigado por esta oportunidade de comentar o artigo entitulado <q>explorando
software criptográfico no repositório main do Debian</q> (em inglês, Exploring
Cryptographic Software in Debian's Main Archive) do Sam Hartman.</p>

<p>Nós estamos fornecendo esta informação para vocês como uma diretriz geral.
O BXA requer que cada entidade que exporta produtos esteja familiarizada, e em
conformidade, com suas obrigações afirmativas estabelecidas pelas Regulações
de Administração de Exportações (Export Administration Regulations). Por favor,
observe que as regulações estão sujeitas à mudança. Recomendamos que você
obtenha seus próprios aconselhamentos legais quando for fazer exportações.
Além disso, alguns países podem restringir certos níveis de encriptação
importados para o país. Recomendamos uma consulta de aconselhamento legal
no país em questão ou na agência governamental aplicável ao país em
particular.</p>

<p>Nesse contexto, a exportação de software criptográfico a partir dos Estados
Unidos é governado sob as Regulações de Administração de Exportações
(<q>EAR</q>, 15 CFR Part 730 et seq.) administrada pelo Escritório de
Administração de Exportações do Departamento de Comércio (<q>BXA - Commerce
Department's Bureau of Export Administration</q>). O BXA revisou as provisões do
EAR aplicáveis aos softwares criptográficos mais recententemente em 19 de
outubro de 2000. Eu me refiro a essas provisões como as <q>novas regulações dos
EUA</q> de modo a distingui-las de regulações anteriores que eram mais
restritivas.</p>

<p>Quando a administração Clinton veio para Washington, a exportação a partir
dos Estados Unidos de itens de encriptação eram controlados de forma semelhante
à <q>munições</q>, sob à Lei de Controle de Exportação de Armas e às Regulações
de Comércio Internacional de Armas. A maior parte das solicitações para
licenças de exportação para itens com forte encriptação foram negadas. A
indústria e grupos de interesse público pressionaram para a liberalização e a
administração Clinton reformou os desatualizados controles de exportação dos
Estados Unidos para itens de encriptação em uma série de passos, culminando
nas novas regulações dos EUA. A nova administração Bush está considerando ir
mais longe nas liberalizações que podem ser publicadas mais ao fim deste
ano.</p>

<p>Apesar dessas liberalizações, os controles de exportação dos Estados Unidos
sobre itens de encriptação comercial permanecem complexos e onerosos. Companhias
dos EUA devem submeter os itens de encriptação para revisões técnicas pelas
agências de inteligência previamente à exportação. As exportações para algumas
agências de governos estrangeiros requerem licenças, como também as exportações
para provedores(as) de telecomunicações e de serviços de internet que desejem
fornecer serviços para algumas agências governamentais. Finalmente,
requerimentos para fiscalização pós-exportação se aplicam para muitas
exportações a partir dos Estados Unidos. Desse modo, os controles de exportação
de encriptação dos Estados Unidos continuam a impor um fardo regulatório
significante para as companhias dos Estados Unidos, retardando a implantação
mundial de criptografia forte em programas de software comercial.</p>

<p>Contudo, nem todos os programas de software com encriptação são produtos
comerciais. Para os propósitos das EAR, controles de código-fonte criptográfico
caem em três categorias: (a) código-fonte aberto, (b) código-fonte comunitário e
(c) código-fonte proprietário. As regras aplicáveis às exportações de cada tipo
de código-fonte são diferentes e elas foram alteradas em importantes pontos nas
novas regulações dos EUA.</p>

<p>Código-fonte aberto se refere ao software que está disponível ao público sem
restrições e livre de cobrança, sob um acordo de licença estilo GNU. O Debian
parece cair nesta categoria. As regulações antigas permitiam a exportação
de código-fonte aberto para qualquer usuário(a) final sem uma revisão técnica,
desde que a pessoa que disponibilizasse o código-fonte aberto preenchesse uma
notificação concomitante ao BXA e à Agência Nacional de Segurança (National
Security Agency - <q>NSA</q>). Contudo, as regulações antigas nada diziam a
respeito às restrições (se houvesse) da exportação de software executável
compilado derivado do código-fonte aberto.</p>

<p>Sob as novas regulações dos EUA, não somente o código-fonte aberto, mas
também o software executável compilado derivado do código aberto, está elegível
para exportação sob as mesmas condições que o código-fonte aberto, desde
que o executável compilado esteja disponível sem restrição e livre de cobrança.
Infelizmente, se você incluir o software executável compilado em um produto
que você distribui por um valor, então o produto resultante está sujeito a todas
as regras que se aplicam aos programas de software comerciais. Por exemplo,
eles devem ser submetidos ao BXA e à NSA para uma revisão técnica única,
descrita acima.</p>

<p>Código-fonte comunitário se refere ao software que está disponível ao público
livre de cobrança para uso não comercial, mas que contém restrições posteriores
de uso comercial. O código-fonte comunitário pode ser exportado essencialmente
sob as mesmas condições que o código-fonte aberto, mas o código comunitário
permanece sujeito a requerimentos de fiscalização mais detalhados.</p>

<p>Código-fonte proprietário se refere a todos os códigos-fonte que não são
<q>abertos</q> e não são <q>comunitários</q>. Exportadores(as) podem
providenciar código-fonte proprietário para qualquer usuário(a) final na
União Europeia e para seus(as) parceiros(as), e para qualquer usuário(a) final
não governamental em outros países, imediatamente após solicitar uma revisão
técnica com o BXA e a NSA. Os mesmos requerimentos de fiscalização aplicáveis ao
código-fonte comunitário também se aplicam ao código-fonte proprietário.</p>

<p>Por favor tenha em mente que as pessoas nos Estados Unidos que podem
postar em sites fora dos Estados Unidos estão sujeitas à lei dos Estados Unidos,
mesmo se o façam a título individual. Portanto, você pode querer avisar as
pessoas nos Estados Unidos que suas postagens para os atuais servidores de
encriptação fora dos Estados Unidos ainda estão sujeitas às regulações dos
Estados Unidos.</p>

<p>Finalmente, você deve estar ciente de que um conjunto crucial de controles
de exportação dos Estados Unidos aplica-se para todas as exportações de
software criptográfico em código-fonte a partir dos Estados Unidos. Em essência,
esses controles proíbem a exportação de software criptográfico em código-fonte
sob a Exceção de Licença TSU (License Exception TSU) para (1) grupos
proibidos (listados em <a href="http://www.bxa.doc.gov/DPL/Default.shtm">http://www.bxa.doc.gov/DPL/Default.shtm</a>),
(2) países proibidos (atualmente Cuba, Irã, Iraque, Líbia, Coreia do Norte,
Sudão, Síria e Afeganistão ocupado pelo Talibã) e (3) projeto, desenvolvimento,
armazenamento, produção ou uso de armas ou mísseis nucleares, químicos ou
biológicos.</p>

<p>Com esse contexto, suas questões específicas a respeito do Debian são
abordadas na ordem em que aparecem no artigo do Sam Hartman, abaixo em
itálico.</p>

<hr />

<h2><i>Explorando software criptográfico no repositório main do Debian</i></h2>

<p><i>Sam Hartman</i></p>

<p><i>Projeto Debian</i></p>

<hrline />

<p style="margin-left: 2em">
    <i>O Debian é um sistema operacional livre. Atualmente, por motivos
    relativos a exportações dos EUA, o Debian divide o software criptográfico em
    um repositório em separado, localizado fora dos EUA. Este documento sumariza
    as questões que nós precisaríamos responder de uma perspectiva legal, de
    modo a fundir esses dois repositórios.</i>
</p>

<hrline />

<h3><i>Sobre o Debian</i></h3>

<p><i>O Debian é um grupo de indivíduos que trabalham para produzir um sistema
operacional livre. Esses indivíduos são responsáveis pelas decisões que tomam
quando trabalhando no Debian; não existe uma organização legal do Debian na
qual os(as) desenvolvedores(as) trabalham ou em favor da qual as decisões são
feitas. Existe uma organização sem fins lucrativos registrada, a Software in the
Public Interest (SPI), que detém dinheiro e recursos para o Debian. Então, as
decisões que os(as) desenvolvedores(as) tomam podem impactar os recursos
pertencentes à SPI e assim impactando a SPI. Outros recursos do Debian
são pertencentes a diversos(as) patrocinadores(as). O Debian geralmente
depende de patrocinadores(as) para conectividade em rede. Também existem
outros grupos terceiros que copiam o software do Debian para espelhos, para que
as pessoas ao redor do mundo possam baixá-los e usá-los. Outras pessoas fazem
e vendem CDs do Debian. Todos esses grupos poderiam ser responsabilizados
em maior ou menor grau pelas decisões que o Debian faz. Nós gostaríamos de
nos conduzir de maneira que minimize a responsabilidade para todas as
partes e, dentro desta limitação, maximizar o valor de nossos esforços.</i></p>

<p><i>Como todos(as) fornecedores(as) de sistemas operacionais, o Debian
precisa incluir software criptográfico. Este software fornece segurança,
permite que usuários(as) se engajem em comércio pela internet e realizem
outras tarefas que requeiram criptografia. Hoje, este software é armazenado
em um servidor fora dos Estados Unidos, conhecido como o <q>servidor non-US</q>.
Atualmente, o Debian não mede esforços para
auxiliar desenvolvedores(as) dos Estados Unidos no acompanhamento das
regulações de controle de exportação se eles(as) fazem upload de software
para o repositório non-US ou para impedi-los(as) de fazer o upload do
software. Gostaríamos de mover o software criptográfico do servidor non-US
para nosso servidor principal nos Estados Unidos.</i></p>

<p><i>Com a crescente natureza conectada do trabalho, e o fato de que mais e
mais funções críticas estão sendo postas em plataformas computacionais, e
o infeliz crescimento de dolo deliberado e maldoso, a segurança vai ser
cada vez mais importante. Criptografia é um importante pilar de uma quantidade
de processos de segurança. Qualquer SO que não faça esforços para continuamente
integrar criptografia, é improvável de ser competitivo.</i></p>

<p><i>Colocar todo o software em uma única fonte, e a correspondente capacidade
de criar um conjunto único de CDs que tenham suporte criptográfico integrado,
facilita para usuários(as), facilita para fornecedores(as) de CD, simplifica a
tarefa de desenvolvedores(as) fazerem upload de software para esses sites, e
simplifica a tarefa de replicar os repositórios de software na internet.</i></p>

<p><i>O restante deste documento focará no servidor principal dentro dos
Estados Unidos e em seus espelhos e cópias ao redor do mundo. É importante
perceber que existe atualmente uma estrutura paralela configurada para lidar
com o servidor non-US.</i></p>

<p><i>A cada poucos meses, os(as) desenvolvedores(as) Debian lançam uma nova
versão oficial do Debian. Este software fica disponível no servidor principal
(e para software criptográfico, no servidor non-US) para um grupo de espelhos
primários ao redor do mundo. Esses espelhos copiam o software do servidor
principal e o torna disponível para usuários(as) e espelhos secundários.
Os(As) usuários(as) podem usar HTTP, FTP ou uma variedade de outros métodos
para recuperar o software. Imagens de CD ficam disponíveis para usuários(as)
e revendedores(as). Essas imagens podem ser queimadas em CDs físicos por
indivíduos ou por aqueles(as) que queiram vender/repassar o Debian.</i></p>

<p><i>Além disso, existem versões constantemente desenvolvidas do Debian: as
versões teste (testing) e instável (unstable). Essas versões são atualizadas
diariamente por desenvolvedores(as) ao redor do mundo. Como a versão oficial,
essas versões ficam disponíveis nos servidores principal e non-US para os
espelhos primários. Os espelhos primários disponibilizam o software via HTTP,
FTP e outros métodos tanto para usuários(as) finais como para espelhos
secundários. Imagens de CD às vezes são feitas dessas versões. A diferença
importante entre essas versões em desenvolvimento e a versão oficial é que elas
estão constantemente em mudança.</i></p>

<p><i>Frequentemente, desenvolvedores(as) fazem upload de binários e código-fonte
ao mesmo tempo. Entretanto, nós suportamos muitos tipos diferentes de computadores,
cada um deles requerendo diferentes binários para o mesmo código-fonte. A maior
parte dos(as) desenvolvedores(as) criam binários somente para uma das
arquiteturas de computador que nós suportamos quando eles(as) fazem upload de um
programa alterado. Processos automáticos pegam e usam o código-fonte que
eles(as) enviaram para construir os binários para as outras arquiteturas.
Dessa maneira, alguns binários para um programa em código-fonte particular
serão provavelmente enviados posteriormente ao seu código-fonte.</i></p>

<p><i>Alguns(as) desenvolvedores(as) Debian também usam os recursos do Debian
para trabalhar em software ainda não lançado. O recurso primário que é útil
para esta tarefa é o servidor CVS do Debian. Neste servidor ficam disponíveis
ao público o código-fonte dos projetos, mas eles podem ser modificados muitas
vezes durante o dia. O servidor CVS está nos Estados Unidos.</i></p>

<p><i>Contudo, a maioria do software do Debian não é desenvolvida diretamente
por desenvolvedores(as) Debian. Ao contrário, o software é lançado publicamente
por terceiros(as). Alguns softwares são disponibilizados para o público em
sites dentro dos EUA, enquanto outros(as) autores(as) originais lançam seu
software em sites fora dos EUA. Os(As) desenvolvedores(as) Debian são
responsáveis por integrar o software no Debian. Como parte deste trabalho,
muitos(as) desenvolvedores(as) Debian acabam trabalhando próximos(as)
ao(à) autor(a) original, frequentemente contribuindo de volta com código para a
versão original.</i></p>

<p><i>O software no Debian está em conformidade com a Definição Debian de Software
Livre; a DFSG. Acreditamos que este software tenha o código-fonte publicamente
disponível no sentido da seção 740.13(e) da EAR. Essas diretrizes requerem que
código-fonte seja redistribuível. A DFSG indiretamente requer que uma pessoa
seja capaz de distribuir um produto baseado no código-fonte sem que pague uma
taxa. Distribuímos todo o código-fonte no Debian como parte dos nossos
lançamentos. Outros softwares são distribuídos em nosso repositório non-free,
mas nosso foco neste documento está no software livre de acordo com a DFSG.
Nós estaríamos interessados(as) em saber em que medida poderíamos mover
o software não DFSG que permite a redistribuição do código-fonte
dentro dos Estados Unidos, mas queremos nos certificar de que os
conselhos nesta área não se confundam com os conselhos de como lidar
com o software livre de acordo com a DFSG.</i></p>

<p><i>Desenvolvedores(as) Debian vivem ao redor do mundo e são cidadãos(ãs)
de muitos países. Obviamente, alguns(as) são cidadãos(ãs) dos EUA, mas
muitos(as) não são. Alguns(as) podem ser cidadãos(ãs) dos sete países
proibidos na seção 740.13(e) da EAR.</i></p>

<p><i>Como mencionado, temos espelhos em todo o mundo. Não temos
qualquer espelho oficial (espelhos nos quais o projeto está conectado) em
qualquer um dos sete países listados na seção 740.13(e) da EAR, embora
nosso software esteja publicamente disponível e assim possa ser copiado para
esses países. Atualmente a maior parte dos espelhos dentro dos Estados
Unidos somente espelha o servidor principal (aquele sem criptografia), embora
alguns espelhem tanto as porções principais quanto a non-US do repositório.
O Debian não assume responsabilidade por espelhos dentro dos EUA que
espelhem a porção non-US do repositório.</i></p>

<hrline />

<h3><i>Nosso objetivo</i></h3>

<p><i>Nós queremos incluir software criptográfico em nosso repositório main.
Nós queremos entender os riscos para desenvolvedores(as), usuários(as), SPI,
mantenedores(as) de espelhos, revendedores(as) de CD, patrocinadores(as) e
quaisquer outras partes que estão conectadas ao Debian para que possamos
fazer uma escolha qualificada. Nós queremos ser capazes de documentar e
publicizar esses riscos para que esses grupos não cometam um crime devido
à ignorância. Obviamente, também queremos evitar a adoção desnecessária
de riscos.</i></p>

<p><i>Em particular, gostaríamos de considerar as seguintes atividades:</i></p>

<ul>
<li><i>De modo cotidiano, adicionar e modificar software livre do tipo DFSG em
    nossos lançamentos. Na prática, somente as versões teste (testing) e
    instável (unstable) são modificadas diariamente, mas outras versões são
    modificadas periodicamente.</i></li>

<li><i>Distribuir software criptográfico como parte de nossos lançamentos pela
    Internet e em CDs.</i></li>

<li><i>Adicionar e alterar software criptográfico livre do tipo DFSG em nosso
    servidor CVS.</i></li>

<li><i>Quaisquer reações que teríamos de mudanças em regulações (ou leis)
     criptográficas.</i></li>
</ul>

<hrline />

<p><em>Fim do preâmbulo do documento do Debian</em></p>

<p>Eu tentarei refletir esses objetivos nas minhas respostas para suas
questões. Como uma forma de resumo, eu penso que uma notificação inicial
deve ser suficiente para o repositório atual e inerentes atualizações. Uma
nova notificação seria requerida somente se um novo programa com encriptação
fosse adicionado ao repositório. Distribuição adicional de freeware não
requer posteriores notificações. Entretanto, versões comerciais seriam
sujeitas a exigências de revisão técnica, licenciamento e prestação
de contas que se aplicam a outros produtos comerciais. A predição de
futuras mudanças nas leis ou regulações é difícil, mas se a lei mudar,
você ou teria que derrubar o site, ou modificá-lo de modo a deixá-lo
em conformidade. Você não tem obrigação de informar outras partes
sobre as obrigações legais delas, mas se você tiver uma lista de perguntas
mais frequentes, eu ficaria feliz em sugerir respostas apropriadas que
você poderia querer oferecer.</p>

<p>Questões (nota, cada pergunta do Debian é marcada com "D:")</p>

<blockquote class="question">
<p>
<span>D:</span>
	Precisamos notificar o Escritório de Administração de Exportações
	(Bureau of Export Administration - BXA) sobre software que adicionamos
        aos lançamentos?
</p>
</blockquote>

<p>Se a notificação é redigida apropriadamente, e o repositório permanece
no site identificado na notificação, então você só tem que registrar uma
notificação única com o BXA para o repositório inicial. Somente uma
notificação para um site nos EUA é requerida; nenhuma notificação em
separado é requerida para sites-espelho dentro ou fora dos EUA. Esta
notificação só teria que ser atualizada se você adicionou um novo programa
implementando encriptação.</p>

<pre>
	Department of Commerce
	Bureau of Export Administration
	Office of Strategic Trade and Foreign Policy Controls
	14th Street and Pennsylvania Ave., N.W.
	Room 2705
	Washington, DC 20230

	Re:  Notificação de código-fonte de encriptação irrestrito
	Commodity:	Código-fonte do Debian

	Caro Senhor/Senhora:
	Consoante com o parágrafo (e)(1) da Parte 740.13 das Regulações de
	Administração de Exportações dos EUA ("EAR", 15 CFR Part 730 et seq.),
	nós estamos providenciando esta notificação escrita da localização na
	internet do código-fonte irrestrito, publicamente disponível do Debian.
	O código-fonte do Debian é um sistema operacional livre desenvolvido por
	um grupo de indivíduos, coordenado pela organização sem fins lucrativos
	Software in the Public Interest. Este repositório é atualizado de
	tempos em tempos, mas sua localização é constante. Portanto, esta
	notificação serve como uma notificação única para atualizações
	subsequentes que podem ocorrer no futuro. Novos programas serão o
	objeto de notificação separada. A localização na internet para o
        código-fonte do Debian é:  https://www.debian.org.

	  Este site é espelhado para uma quantidade de outros sites localizados
	fora dos Estados Unidos.

	  Uma cópia desta notificação foi enviada para o ENC
	Encryption Request Coordinator, P.O. Box 246, Annapolis Junction, MD
	20701-0246.

	  Se você tiver quaisquer questões, por favor me contate em (xxx) xxx-xxxx.

	Atenciosamente,
	  Nome
	  Título
</pre>


<blockquote class="question">
<p>
<span>D:</span>
	Quais informações precisamos incluir nas notificações?
</p>
</blockquote>

<p>O esboço da linguagem acima inclui a informação que você precisa para
incluir na notificação</p>


<blockquote class="question">
<p>
<span>D:</span>
	Com que frequência precisamos notificar? Nós queremos notificar o
	menos possível já que isso impõe mais trabalho para nós e para o
	governo, mas nós queremos notificar o quanto for necessário para seguir
	as regulações.
</p>
</blockquote>

<p>Como no rascunho acima, e assumindo que o repositório permaneça no site da
internet identificado na notificação, você não deve ter que registrar uma
notificação subsequente para atualizações subsequentes. Você somente
registraria outra notificação se você tivesse adicionado um novo programa
implementando criptografia.</p>


<blockquote class="question">
<p>
<span>D:</span>
        Se movermos nosso software criptográfico para este país,
        e as leis ou regulações mudarem para serem mais restritivas, o que
        nós temos a perder? Teríamos que destruir algum software,
        ou CDs? Teríamos que removê-los do nosso site primário ou
        secundário? Se usarmos a crescente disponibilidade de software
        criptográfico para melhorarmos a segurança do resto do sistema, e
        o ambiente legal criptográfico piorar, pode acontecer de
        termos que descartar todas as cópias de tal software nos EUA?
</p>
</blockquote>

<p>A tendência tem sido de liberalização crescente de controles de
exportação de criptografia nos Estados Unidos, contrariamente ao aumento
de restrições. Essa tendência tem sido constante na década passada e tem
se acelerado no último ano. Não podemos lhe aconselhar a respeito de
o que poderia perder, a menos que, e até que, novas regulações sejam publicadas.
Contudo, nós acreditamos que você reteria o copyright para o software e alguns,
embora talvez mais limitados, direitos de exportação.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Em ordem decrescente de preferência, gostaríamos de notificar:
        </p>
	<ul>
	<li>Uma vez para todo o repositório Debian</li>

	<li>Uma vez para cada lançamento oficial (tendo em mente que
	  teste/instável (testing/unstable) mudam entre lançamentos)</li>

	<li>Uma vez quando um novo programa contendo criptografia é adicionado
	  ao repositório</li>

	<li>Uma vez quando uma nova versão do programa contendo criptografia
	  é adicionado ao repositório.</li>
	</ul>

</blockquote>

<p>Eu acho que você só tem que registrar uma nova notificação se você adicionar
um novo programa que incorpora criptografia. Atualizações para programas
existentes devem estar cobertas pela linguagem abrangente da notificação que
sugerimos acima.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Novos pacotes entram no repositório Debian através da seguinte sequência
	de etapas. Em que ponto a notificação deve acontecer?
</p>
	<ol>
	<li>Desenvolvedor(a) original (upstream) lança um pacote como código
	   aberto. Este passo é ignorado no caso de um pacote nativo do
           Debian.</li>
	<li>Um(a) desenvolvedor(a) Debian empacota o código-fonte e o binário
	   para o Debian, frequentemente com mudanças no código-fonte.</li>
	<li>O pacote é enviado para ftp-master, repositório de entrada
           (incoming).</li>
	<li>O novo pacote falha ao instalar, porque é novo.</li>
	<li>Administradores(as) do ftp adicionam os registros necessários para o
           pacote.</li>
	<li>O pacote é instalado no repositório dentro de poucos dias.</li>
	<li>O pacote é copiado para os sites de espelhamento.</li>
	</ol>
</blockquote>

<p>As regulações são bem claras sobre as notificações terem que ocorrer
antes de, ou concomitante com, a disponibilidade pública. Exportações anteriores
à disponibilidade pública requerem uma licença de exportação. Portanto, se o
repositório na etapa 3 não está publicamente disponível, então ou o pacote deve
ser colocado publicamente disponível antes da etapa 3 (e as notificações
enviadas), ou as licenças de exportação serão necessárias para os(as)
desenvolvedores(as) Debian. Se o repositório na etapa 3
está publicamente disponível, então a notificação neste ponto eliminaria a
necessidade de ter licenças de exportação para desenvolvedores(as) Debian.</p>


<blockquote class="question">
<p>
<span>D:</span>
        Se o(a) autor(a) original (upstream) notificou o BXA, a notificação é
        necessária? (O empacotamento para o Debian pode envolver modificações
        no código-fonte envolvendo localização de arquivos, e ocasionalmente
        diferenças funcionais, embora o objetivo geral seja fazer o código do(a)
        autor(a) original funcionar no Debian com modificações mínimas).
</p>
</blockquote>

<p>Se o(a) autor(a) original notificou o BXA, isto é suficiente.</p>


<blockquote class="question">
<p>
<span>D:</span>
        Nós temos que notificar quando novos binários (código-objeto) são
        adicionados, se nós já tivermos notificado para o código-fonte?
</p>
</blockquote>

<p>Eu não acho que você tenha que registrar uma nova notificação para o
código-objeto, dado que a notificação para o código-fonte já foi registrada.</p>


<blockquote class="question">
<p>
<span>D:</span>
        A notificação é requerida para programas que não contêm algoritmos de
        criptografia, mas são vinculados com bibliotecas criptográficas? E sobre
        programas que iniciam outros programas de modo a realizar funções
        criptográficas?
</p>
</blockquote>

<p>Enquanto o programa for código aberto, ele pode incluir uma API criptográfica
aberta e ainda assim qualificar-se sob Exceção de Licença TSU (License Exception
TSU).</p>


<blockquote class="question">
<p>
<span>D:</span>
        Novos programas podem ser facilmente verificados antes de seu lançamento
        (e a notificação feita naquele momento), mas quando uma atualização é
        feita, não existe um passo manual quando possa ser feita a notificação.
        Seria aceitável notificar o BXA para cada adição de software, com uma
        indicação de que futuras atualizações podem incluir a adição de
        funcionalidades criptográficas?
</p>
</blockquote>

<p>Sim. O exagero no relato provavelmente deve ser evitado onde for razoável,
mas subnotificações devem ser evitadas. Atualizações futuras de um programa
existente não requerem notificações separadas. Somente novos programas requerem
notificações separadas.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Podemos automatizar o processo de envio de notificação?
</p>
</blockquote>

<p>Você pode automatizar o processo de envio de notificações. Isto é um
assunto procedimental interno. O BXA e a NSA não se importam como você
administra o registro de notificações internamente.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Que forma deve ter a notificação?
</p>
</blockquote>

<p>A notificação BXA deve estar ou em formato eletrônico, ou impresso;
entretanto, a notificação NSA deve estar em formato impresso.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Quem pode enviar as notificações? Por exemplo, eles(as) precisam ser
	cidadãos(ãs) dos EUA?
</p>
</blockquote>

<p>Qualquer pessoa pode enviar as notificações; a cidadania não é relevante.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Existem outras preocupações que devemos estar cientes? Que outros
	passos além da notificação temos que realizar?
</p>
</blockquote>

<p>Além de notificação, você poderia considerar implementar uma busca reversa
de IP que identifique o computador que requisita o download, e que bloqueia
o download do repositório criptográfico para países embargados pelos
Estados Unidos: Cuba, Irã, Iraque, Líbia, Coreia do Norte, Síria, Sudão e
Afeganistão ocupado pelo Talibã. Além disso, você poderia considerar
ter um provisão em seu acordo de licenciamento, ou uma tela em separado
antes do download, que avisa para a pessoa que está fazendo o download do
software o seguinte:</p>

<p>Este software está sujeito aos controles de exportação dos EUA aplicáveis
ao software de código aberto que inclui encriptação. O Debian registrou a
notificação com o Escritório de Administração de Exportações (Bureau of Export
Administration) e com a Agência Nacional de Segurança (National Security Agency)
como requerido antes de exportações sob as provisões da Exceção de Licença TSU
(License Exception TSU) das Regulações de Administração de Exportações dos
EUA. Consistente com os requerimentos da Exceção de Licença TSU, você representa
e garante que você é elegível para receber este software, e que você não está
localizado em um país sujeito ao embargo pelos Estados Unidos, e que você não
usará o software diretamente ou indiretamente para projetar, desenvolver,
estocar ou usar armas ou mísseis nucleares, químicos ou biológicos. Código
binário compilado que é fornecido sem custos pode ser reexportado sob as
provisões da Exceção de Licença TSU. Contudo, revisão técnica adicional
e outros requerimentos podem se aplicar para produtos comerciais que incorporem
este código, antes da exportação a partir dos Estados Unidos. Para informações
adicionais, por favor, refira-se a www.bxa.doc.gov.</p>


<blockquote class="question">
<p>
<span>D:</span>
        Atualmente, usuários(as) ao redor do mundo podem acessar e
        potencialmente baixar software que está aguardando integração em nosso
        repositório. Provavelmente, nós faríamos qualquer notificação necessária
        ao que o software é processado em nosso repositório, então o software
        neste estado estaria aguardando notificação. Isto seria um problema?
        Em caso afirmativo, seria aceitável estabelecer uma fila alternativa de
        software criptográfico aguardando integração no repositório,
        disponível somente para nossos(as) desenvolvedores(as)? De modo a
        processar o software em nossa distribuição, os(as) desenvolvedores(as)
        que frequentemente estão fora dos EUA precisam examinar o software e
        se certificar de que seguem certas diretrizes. Como realizaríamos
        este acesso? Existem outras soluções para esta área pré-notificação
        do repositório que nós devemos considerar?
        </p>
	<p>Uma questão que frequentemente nos ocorre é patente de software.
        Claramente, a integração de criptografia no software não remove qualquer
        das preocupações de patente que normalmente teríamos que pensar a
        respeito. Contudo, existem novos problemas que precisamos considerar
        quando as patentes interagem com regulações de exportação de
        criptografia? Parece que ao menos para a exceção TSU (seção 740.13 da
        EAR), as patentes não influenciam se o código-fonte é público

</p>
</blockquote>

<p>É importante diferenciar entre o repositório que foi objeto de
notificação e novos programas. Você pode atualizar o repositório
que foi objeto de notificação sem notificação adicional, como
descrito acima. Somente novos programas precisam ser sujeitos à notificação
em separado, anteriormente à postagem. Se novos programas precisam ser
revistos por desenvolvedores(as) antes da postagem, e tal software não
está publicamente disponível e nem foi notificado ao governo dos EUA, então
eu recomendo que você considere obter uma licença de exportação autorizando
esta revisão limitada, pré-notificação. Você está correto(a) sobre as patentes
não desqualificarem o software para elegibilidade de exportação sob a
Exceção de Licença TSU.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Distribuição, espelhamento e CDs</p>

	<p>Nossos espelhos dentro dos EUA precisam notificar o BXA se nós
        adicionarmos criptografia em nosso repositório? Com que frequência
        eles precisam notificar o BXA? Nós gostaríamos de evitar uma situação
        em que os espelhos tenham que notificar por cada programa que o Debian
        adiciona ao repositório, mesmo que nosso servidor principal deva enviar
        tais notificações. Precisamos manter as operações simples para
        os(as) operadores(as) de espelhos. O quê, se existir algo, os espelhos
        fora dos EUA precisam fazer?</p>

	<p>Se enviarmos uma atualização para um espelho em vez de esperar
        que ele faça o download do software, precisamos fazer algum passo
        especial? E se enviarmos para um espelho uma requisição para baixar
        software novo/alterado?

</p>
</blockquote>

<p>Uma vez que a notificação foi registrada para o servidor central, nenhuma
notificação adicional é requisitada para sites-espelho.</p>


<blockquote class="question">
<p>
<span>D:</span>
        Quais dos(as) seguintes fornecedores(as) (se existirem) seriam capazes
        de enviar os binários do Debian não modificados (e código-fonte) somente
        com a notificação? Quais deles(as) requerem revisão e aprovação?
        Poderia a revisão ser concomitante ao envio, ou a aprovação deve
        anteceder o envio?
</p>
	<p>
	A) envio por encomenda postal de CDs pelo custo da mídia?<br />
	B) envio por encomenda postal de CDs com lucro?<br />
	C) vendas avulsas de CDs pelo custo da mídia?<br />
	D) vendas avulsas de CDs com lucro?<br />
	E) fornecedor(a) fornecendo CDs de A ou C acima, junto com hardware.
	   Hardware vendido com lucro, mas sem pré-instalação?<br />
	F) E, mas com software pré-instalado?<br />
	G) qualquer um acima, vendendo suporte para o software?
	</p>

	<p>Se for mais fácil, uma outra forma de ver isso é: que condições
        precisam ser satisfeitas para que o(a) fornecedor(a) envie os binários
        sob a Exceção de Licença TSU, e quais despesas o(a) fornecedor(a) tem
        permissão de recuperar como custo e/ou como venda com lucro?</p>
</blockquote>

<p>Taxas razoáveis e comuns para reprodução e distribuição são
permitidas, mas não taxas de licenciamento ou royalties. Suporte também é
permitido, sujeito à limitação acima.</p>


<blockquote class="question">
<p>
<span>D:</span>
        Se a revisão única é requerida para binários não modificados enviados
        com lucro, pode essa aprovação ser usada por outros(as) fornecedores(as)
        que estão enviando binários não modificados?
</p>
</blockquote>

<p>A revisão feita uma única vez é para o produto, e é independente do(a)
fornecedor(a).</p>


<blockquote class="question">
<p>
<span>D:</span>
        Seria aceitável estabelecer um espelho oficial em um país
        proibido no EAR seção 740.13(e)?
</p>
</blockquote>

<p>Você teria que solicitar uma licença para estabelecer um espelho oficial em
um país embargado.</p>


<blockquote class="question">
<p>
<span>D:</span>
        Se é tecnicamente impraticável bloquear o acesso de países T7 a
        servidores web (ou ftp, etc.), uma diligência prévia vai requerer
        medidas extremas? O padrão realmente existente das práticas comuns
        da indústria (dos EUA) segue essas diligências prévias?
</p>
</blockquote>

<p>O padrão existente da indústria deve ser suficiente. Eu espero que o
governo reconhecerá que qualquer sistema criado pela humanidade pode ser
derrotado com algum esforço.</p>


<blockquote class="question">
<p>
<span>D:</span>
        Que passos devemos tomar se ficarmos cientes de que alguém está fazendo
        o download de software para aqueles países a partir de um espelho de
        dentro dos Estados Unidos? E se ficarmos cientes de downloads para
        um daqueles países a partir de um espelho fora dos Estados Unidos?
</p>
	<p>Alguns(as) de nossos(as) desenvolvedores(as) podem viver ou serem
        cidadãos(ãs) dos sete países proibidos pela exceção TSU. Seria um
        problema para esses(as) desenvolvedores(as) ter acesso ao software
        criptográfico em nossas máquinas? Nós precisaríamos pedir a eles(as)
        para não baixar tais softwares? Que passos devemos fazer se
        ficarmos cientes de que eles(as) estão baixando software
        criptográfico?</p>
</blockquote>

<p>Simplesmente postar software criptográfico em um servidor que pode ser
acessível de um país embargado não constitui <q>conhecimento</q> de que o
software foi exportado para lá. Portanto, a responsabilidade criminal não
se aplicaria para o ato de postagem. Recomendamos que você realize
uma verificação de IP e bloqueie os downloads para países conhecidamente
embargados. Este procedimento também forneceria uma defesa para uma ação
de responsabilidade civil. Se você descobrir que seu software foi baixado para
um destino proibido, então eu recomendo que você bloqueie futuros downloads
para aquele site específico a menos, e até que, você obtenha uma licença
do BXA.</p>

<hr />

<p>O Debian agradece à <a href="http://www.hp.com/">Hewlett-Packard</a>
<a href="http://www.hp.com/products1/linux/">Linux Systems Operation</a>
pelo seu apoio em obter essa opinião legal.</p>
