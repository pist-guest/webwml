#use wml::debian::template title="AMD64 Port"
#use wml::debian::translation-check translation="c31fd1581b29fe64394cf736d6444851bba82a0f" maintainer="Sebul"


#use wml::debian::toc

<toc-display/>

<toc-add-entry name="about">AMD64에 데비안</toc-add-entry>
<p>
이 페이지는 AMD64 아키텍처에서 Debian GNU/리눅스를 실행하는 사용자와 데비안 개발자들을 돕기 위한 것입니다.
여기서 개발자들이 공개적으로 접근할 수 있는 기계, 포트개발을 논의할 장소, 데비안 포터들에 대한 추가 정보를 얻을 수 있는 장소, 그리고 더 많은 정보를 얻을 수 있는 포인터들에 대한 정보를 찾을 수 있습니다.</p>

<toc-add-entry name="status">현재 상태</toc-add-entry>
<p>AMD64는 데비안 4.0 릴리스 이래 데비안 아키텍처에서 공식적으로 지원됩니다.
</p>

<p>포트는 <em>AMD64</em> 확장성을 가진 모든 AMD64 64비트 CPU와 
<em>Intel 64</em> 확장성을 가진 모든 Intel CPU에 대한 커널 및 
공통 64비트 사용자 공간으로 구성됩니다.
</p>

<toc-add-entry name="features">완전한 64비트 사용자 영역</toc-add-entry>
<p>AMD64 포트는 철저하게 64비트이므로, 사용자는이 아키텍처는 i386에 비해 모든 이점을 활용할 수 있습니다:
</p>
<ul>
<li>낮은 메모리와 높은 메모리로 메모리 세분화 없음</li>
<li>프로세스 당 최대 128TiB 가상 주소 공간 (2GiB 대신)</li>
<li>4GiB (또는 PAE 확장명을 가진 64GiB) 대신 64TiB 물리적 메모리 지원</li>
<li>CPU 안에 8 대신 16 범용 레지스터</li>
<li>gcc는 387 FPU 대신 SSE2 math로 기본 설정</li>
<li>gcc 는 frame-pointer를 기본으로 -O2에서 생략</li>
<li>컴파일 시간 최적화는 기존 i386 cruft 대신 AMD64/인텔 64의 공통 기반을 사용</li>
<li>메모리 페이지는 실행 가능하지 않은 것이 기본값</li>
</ul>

<p>레거시 32 비트 바이너리의 기본 실행은 커널에서 지원되며, 
필요한 핵심 라이브러리는 데비안의 <a
href="https://wiki.debian.org/Multiarch">Multiarch 메커니즘</a>을 통해 제공됩니다.
</p>


<toc-add-entry name="i386support">i386에 대한 최소 AMD64 런타임 지원
</toc-add-entry>
<p>공식 i386 배포판에는 64비트 커널, 64비트 바이너리를 생성할 수 있는 툴체인 및 amd64-libs 패키지로 구성된 
최소 AMD64 지원이 기본 공유 라이브러리와 함께 타사 amd64 바이너리를 실행합니다.
</p>


<toc-add-entry name="ml">메일링 리스트</toc-add-entry>

<p>이 포트에 대한 토론과 개발은 <a
href="https://lists.debian.org/debian-amd64/">debian-amd64</a>
리스트에서 이루어집니다.</p>


<toc-add-entry name="publicmachines">Public machines</toc-add-entry>

<p>데비안 멤버는 데비안 <a href="https://db.debian.org/machines.cgi">porterbox machines</a>을 써서
패키지를 포팅할 수 있습니다.
</p>

<toc-add-entry name="links">링크</toc-add-entry>

<ul>
<li><a href="https://wiki.debian.org/DebianAMD64">debian-amd64 위키</a></li>
</ul>

