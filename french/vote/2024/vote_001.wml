<define-tag pagetitle>Élection du responsable du projet Debian 2024</define-tag>
<define-tag status>F</define-tag>
# signification des balises <status> :
# P: proposé
# D: débattu
# V: voté
# F: terminé
# O: autre (ou indiquez simplement autre chose)

#use wml::debian::translation-check translation="944130896815d658b0d1b45673005bb978b2194c" maintainer="Jean-Pierre Giraud"
#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />



# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


   <vtimeline />
     <table class="vote">
         <tr>
           <th>Période de candidature :</th>
           <td>samedi 9 mars 2024 00:00:00 UTC</td>
           <td>vendredi 15 mars 2024 23:59:59 UTC</td>
         </tr>
         <tr>
           <th>Période de campagne :</th>
           <td>samedi 16 mars 2024 00:00:00 UTC</td>
           <td>vendredi 5 avril 2024 23:59:59 UTC</td>
         </tr>
         <tr>
           <th>Période de scrutin :</th>
           <td>samedi  6 avril 2024 00:00:00 UTC</td>
           <td>vendredi 19 avril 2024 23:59:59 UTC</td>
         </tr>
     </table>
     <p>Veuillez noter que le nouveau mandat du responsable du projet débutera
        le 21 avril 2024.</p>

          <vnominations />
            <ol>
		<li>Andreas Tille [<email tille@debian.org>] [<a href='https://lists.debian.org/debian-vote/2024/03/msg00002.html'>message de candidature</a>] [<a href="platforms/tille">programme</a>]
		<li>Sruthi Chandran [<email srud@debian.org>] [<a href='https://lists.debian.org/debian-vote/2024/03/msg00005.html'>message de candidature</a>] [<a href="platforms/srud">programme</a>]
            </ol>
             
             <p>
                Les bulletins, quand ils sont prêts, peuvent être demandés
                en envoyant un courrier électronique signé à
                <a href="mailto:ballot@vote.debian.org">ballot@vote.debian.org</a>
                avec pour sujet leader2024.
             </p>

          <vstatistics />
          <p>
            Cette année, comme d'habitude, des
#		<a href="https://vote.debian.org/~secretary/leader2024/">statistiques</a>
		<a href="suppl_001_stats">statistiques</a>
		sur les bulletins et les accusés de réception seront rassemblées
		périodiquement durant la période du scrutin.
		De plus, la liste des <a href="vote_001_voters.txt">votants</a> sera
		enregistrée. 
		La <a href="vote_001_tally.txt">feuille d'émargement</a> sera
		également disponible. Veuillez noter que l'élection du responsable
		du projet se fait à bulletins secrets, la feuille d'émargement ne
		contiendra donc pas le nom des votants mais un HMAC qui permet aux
		votants de vérifier que leur vote est dans la liste. Une clef est
		générée pour chaque votant, et envoyée avec l'accusé de réception
		de leur bulletin.
         </p>


          <vquorum />
     <p>
        Avec la liste actuelle des <a href="vote_001_quorum.log">développeurs
        votants</a>, nous avons&nbsp;:
     </p>
     <pre>
#include 'vote_001_quorum.txt'
    </pre>
#include 'vote_001_quorum.src'


          <vmajorityreq />
          <p>Le candidat a besoin d'une majorité simple pour être élu.</p>

#include 'vote_001_majority.src'


          <voutcome />
#include 'vote_001_results.src'

    <hrline>
      <address>
        <a href="mailto:secretary@debian.org"> Secrétaire du Projet Debian</a>
      </address>
