#use wml::debian::cdimage title="Installation par le réseau à partir d'une clé USB ou d’un CD minimaux"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"

#use wml::debian::translation-check translation="678a572e92767d98d57f67822e00905782bf9637" maintainer="Jean-Paul Guillonneau"
# Original translation by Willy Picard
# Previous translation by Thomas Huriaux
# differents translators : see the log file
# Jean-Paul Guillonneau, 2016-2021

<div class="tip">
<p>Sur les architectures i386 et amd64, toutes les images USB/CD/DVD peuvent
aussi être utilisées avec
<a href="https://www.debian.org/CD/faq/#write-usb">une clé USB</a>.</div>

<p>Un CD d'installation par le réseau, ou CD
«&nbsp;netinst&nbsp;» est un CD unique qui vous permet d'installer
le système d'exploitation complet. Ce simple CD contient seulement
les logiciels nécessaires pour installer le système de base et
ensuite récupérer les paquets restants depuis Internet.</p>

<p>
<strong>Quels types de connexion réseau sont gérés pendant
l'installation&nbsp;?</strong>
Différents moyens sont permis pour cela, comme Ethernet et WLAN (avec quelques
limitations).
</p>

<p>Les images minimales de CD amorçable suivantes sont disponibles
en téléchargement&nbsp;:</p>

<ul>
  <li>images officielles «&nbsp;d'installation par le réseau&nbsp;»
  pour la publication «&nbsp;stable&nbsp;»
  &ndash;&nbsp;<a href="#netinst-stable">voyez ci-dessous</a>&nbsp;;</li>

<li>images pour la publication « testing » &ndash;&nbsp;consultez la page de
l’<a href="$(DEVEL)/debian-installer/">installateur Debian</a>.</li>
</ul>

<h2 id="netinst-stable">Images officielles d'installation par le réseau
pour la publication «&nbsp;stable&nbsp;»</h2>

<p>
Cette image fournit l'installateur et un petit ensemble de paquets qui
permettent l'installation d'un système (vraiment) de base.</p>

<div class="line">
<div class="item col50">
<p><strong>
Images de CD d'installation par le réseau
</strong></p>
   <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>
Images de CD d'installation par le réseau (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)
</strong></p>
   <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>Pour plus d'informations sur ce que sont ces fichiers et comment
les utiliser, veuillez consulter la <a href="../faq/">FAQ</a>.</p>

<p>Veuillez vous assurer d'avoir lu les
<a href="$(HOME)/releases/stable/installmanual">informations
détaillées concernant la procédure d'installation</a>.</p>

<h2><a name="firmware">Images non officielles de CD ou DVD avec des
microprogrammes non libres inclus</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Si n’importe quel composant matériel de votre système <strong>exige le
chargement d’un microprogramme non libre</strong> avec le pilote de
périphérique, il est possible d’utiliser une des
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archives de paquet de microprogrammes les plus courants</a> ou télécharger une
image <strong>non officielle</strong> incluant ces microprogrammes <strong>non
libres</strong>. Les instructions sur la façon d’utiliser ces archives et des
informations générales sur la manière de charger un microprogramme lors de
l’installation peuvent être trouvées dans le
<a href="../../releases/stable/amd64/ch06s04">Manuel d’installation</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">Images d’installation non officielles pour la distribution <q>stable</q>
avec des microprogrammes inclus</a>
</p>
</div>
