#use wml::debian::cdimage title="Images d'installation autonomes"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="7791836a42fa0b102a9f31cfc78e2c4345504388" maintainer="Jean-Paul Guillonneau"

# Translators:
# David Prévot, 2011-2014.
# Jean-Paul Guillonneau, 2015-2024

<p>
Une image d'installation autonome (<q>live</q>) permet de démarrer
un système Debian sans modifier un seul fichier du disque dur et
permet aussi d'installer Debian à partir du contenu de l'image.
</p>

<p>
<a name="choose_live"><strong>Une image autonome me conviendrait-elle ?</strong></a>

Voici quelques aspects à prendre en considération pour vous aider à en juger.
</p>

<ul>
<li><b>Alternatives :</b> les images autonomes sont proposées dans plusieurs
variantes, proposant le choix de l’environnement de bureau (GNOME, KDE, LXDE,
Xfce, Cinnamon ou MATE).
<li>
<b>Architecture :</b> actuellement, seules les images pour l’architecture
PC 64 bits (amd64) la plus répandue sont proposées.
</li>
<li>
<b>Installateur :</b> les images autonomes fournissent
l’installateur convivial <a href="https://calamares.io">Calamares</a>, un
cadriciel d’installation indépendant de la distribution.
</li>
<li>
<b>Langues :</b> les images ne sont pas livrées avec un ensemble complet de
paquets régionalisés.

Si vous avez besoin de méthodes de saisie, de fontes ou de paquets de
langues supplémentaires, il vous faudra les installer par la suite.
</li>
</ul>
</div>

<div class="line">
<div class="item col50">
<h2 id="live-install-stable">
Images d'installation autonome officielles pour la distribution <q>stable</q>
</h2>

<p>
Ces images permettent d’essayer un système Debian et de l'installer à
partir du même média. Elles peuvent être inscrites sur des clés USB ou des
disques DVD-R(W).
</p>
    <ul class="quicklist downlist">
      <li><a title="Download Gnome live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Gnome autonome</a></li>
      <li><a title="Download Xfce live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Xfce autonome</a></li>
      <li><a title="Download KDE live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">KDE autonome</a></li>
      <li><a title="Download other live ISO for 64-bit Intel and AMD PC"
            href="<live-images-url/>/amd64/iso-hybrid/">Autres ISO autonomes</a></li>
      <li><a title="Download live torrents for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/bt-hybrid/">Torrents autonomes</a></li>
    </ul>

</div>

<div class="item col50">
<h2 id="live-install-testing">
Images d'installation autonome officielles pour la distribution <q>testing</q>
</h2>

<p>
Ces images permettent d’essayer un système Debian et de l'installer à
partir du même média. Elles peuvent être inscrites sur des clés USB ou des
disques DVD-R(W).
</p>

    <ul class="quicklist downlist">
      <li>IMAGES de TESTING :</li>
      <li><a title="Download Gnome live ISO for 64-bit Intel and AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-gnome.iso">Gnome autonome</a></li>
      <li><a title="Download Xfce live ISO for 64-bit Intel and AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-xfce.iso">Xfce autonome</a></li>
      <li><a title="Download KDE live ISO for 64-bit Intel and AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-kde.iso">KDE autonome</a></li>
      <li><a title="Download other live ISO for 64-bit Intel and AMD PC"
            href="<live-images-testing-url/>/amd64/iso-hybrid/">Autres ISO autonomes</a></li>
    </ul>

</div>

<p>
Pour plus de renseignements à propos de ces fichiers et de la façon
de les utiliser, veuillez consulter la <a href="../faq/">FAQ</a>.
</p>

<p>
Veuillez consulter la <a href="$(HOME)/devel/debian-live">page
du projet Debian Live</a> pour obtenir plus de renseignements
sur les systèmes Debian autonomes fournis par ces images.
</p>
