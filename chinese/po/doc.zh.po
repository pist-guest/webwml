#
# Yangfl <mmyangfl@gmail.com>, 2017.
# Wenbin Lv <wenbin816@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2024-03-04 20:03+0800\n"
"Last-Translator: Wenbin Lv <wenbin816@gmail.com>\n"
"Language-Team: 汉语 <debian-chinese-gb@lists.debian.org>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.4.2\n"

#: ../../english/doc/books.data:35
msgid ""
"\n"
"  Debian 9 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""
"\n"
"Debian 9 是学习 Linux 的必备手册。从初学者开始，学习如何使用图形界面和终端部"
"署系统。本书提供了成长为“初级”系统管理员的基础知识。从探索 GNOME 桌面环境开"
"始，并根据您的个人需求进行调整。克服对使用 Linux 终端的恐惧，学习管理 Debian "
"的最基本命令。增加您对系统服务（systemd）的了解并学习如何调整它们。充分利用 "
"Debian 中和 Debian 之外的软件。使用 network-manager 等管理您的家庭网络。本书 "
"10% 的利润将捐赠给 Debian 项目。"

#: ../../english/doc/books.data:63 ../../english/doc/books.data:173
#: ../../english/doc/books.data:228
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""
"这本免费书籍由两位 Debian 开发人员撰写，最初是他们的法语畅销书 Cahier de "
"l'admin Debian（由 Eyrolles 出版）的翻译版。本书面向所有人开放，向任何想要成"
"为有效且独立的 Debian GNU/Linux 管理员的人传授基本知识。它涵盖了一个称职的 "
"Linux 管理员应该掌握的所有主题，从安装和更新系统，到构建软件包和编译内核，以"
"及监控、备份和迁移，同时也没有忘记像配置 SELinux 以保护服务、自动化安装、使"
"用 Xen、KVM 或 LXC 进行虚拟化等等高级主题。"

#: ../../english/doc/books.data:85
msgid ""
"The aim of this freely available book is to get you up to\n"
"  speed with Debian. It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school). Some Debian specific information "
"are very old.\n"
"  "
msgstr ""
"这本免费书籍的目的是让您快速了解 Debian。它为自行安装和维护系统的用户（无论是"
"在家庭、办公室、俱乐部还是学校）提供全面的基本支持。 一些关于 Debian 的信息十"
"分过时。"

#: ../../english/doc/books.data:107
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""
"第一本关于 Debian 的法语书籍已经出版到第五版了。它涵盖了 Debian 管理的各个方"
"面，从安装到网络服务配置。这本书由两位 Debian 开发人员撰写，可能会引起很多人"
"的兴趣：希望了解 Debian 的初学者、寻找精通 Debian 相关工具所需要的技巧的高级"
"用户，以及想要使用 Debian 构建可靠网络的管理员。"

#: ../../english/doc/books.data:127
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""
"本书涵盖的主题范围广泛，从使用可用工具进行软件包管理的概念、如何使用它们解决"
"实际使用中可能出现的具体问题，以及如何解决这些问题。这本书是用德语写的，计划"
"翻译成英语。格式：电子书（在线、HTML、PDF、ePub、Mobi），计划出版纸质书籍。\n"

#: ../../english/doc/books.data:148
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""
"本书教授用户如何安装和配置系统，以及如何在专业环境下使用 Debian。它展示了这个"
"发行版（当前系统版本为第 8 版）的完整潜力，并提供了帮助所有想要了解 Debian 及"
"其一系列服务的用户的实用手册。"

#: ../../english/doc/books.data:202
msgid ""
"Written by two penetration researcher - Annihilator, Firstblood.\n"
"  This book teaches you how to build and configure Debian 8.x server system\n"
"  security hardening using Kali Linux and Debian simultaneously.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 webserver, etc.\n"
"  From the perspective that 'the origin of Kali Linux is Debian', it "
"explains\n"
"  how to enhance Debian's security by applying the method of penetration\n"
"  testing.\n"
"  This book covers various security issues like SSL cerificates, UFW "
"firewall,\n"
"  MySQL Vulnerability, commercial Symantec antivirus, including Snort\n"
"  intrusion detection system."
msgstr ""
"由两位渗透研究员——Annihilator、Firstblood 撰写。本书教您如何同时使用 Kali "
"Linux 和 Debian 构建和配置强化了安全性的 Debian 8.x 服务器系统。DNS、FTP、"
"SAMBA、DHCP、Apache2 webserver 等。从“Kali Linux 源于 Debian”的角度，阐述了如"
"何应用渗透测试的方法来增强 Debian 的安全性。本书涵盖了各种安全问题，如 SSL 证"
"书、UFW 防火墙、MySQL 漏洞、商业 Symantec 防病毒软件，包括 Snort 入侵检测系"
"统。"

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "作者："

#: ../../english/doc/books.def:41
msgid "Debian Release:"
msgstr "Debian 版本："

#: ../../english/doc/books.def:44
msgid "email:"
msgstr "電子郵箱："

#: ../../english/doc/books.def:48
msgid "Available at:"
msgstr "售於："

#: ../../english/doc/books.def:51
msgid "CD Included:"
msgstr "包括光碟："

#: ../../english/doc/books.def:54
msgid "Publisher:"
msgstr "發行商："

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "作者："

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr "編輯："

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr "維護者："

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr "狀態："

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr "获得途径："

#: ../../english/doc/manuals.defs:85
msgid "Latest version:"
msgstr "最新版本："

#: ../../english/doc/manuals.defs:101
msgid "(version <get-var version />)"
msgstr "(version <get-var version />)"

#: ../../english/doc/manuals.defs:137 ../../english/releases/arches.data:40
msgid "plain text"
msgstr "純文字"

#: ../../english/doc/manuals.defs:140
msgid "HTML (single page)"
msgstr "HTML（单页）"

#: ../../english/doc/manuals.defs:156 ../../english/doc/manuals.defs:166
#: ../../english/doc/manuals.defs:174
msgid ""
"The latest <get-var srctype /> source is available through the <a "
"href=\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""
"最新的 <get-var srctype /> 格式的源代码可由 <a href=\"https://packages."
"debian.org/git\">Git</a> 仓库获得。"

#: ../../english/doc/manuals.defs:158 ../../english/doc/manuals.defs:168
#: ../../english/doc/manuals.defs:176
msgid "Web interface: "
msgstr "网页界面："

#: ../../english/doc/manuals.defs:159 ../../english/doc/manuals.defs:169
#: ../../english/doc/manuals.defs:177
msgid "VCS interface: "
msgstr "VCS 界面："

#: ../../english/doc/manuals.defs:184 ../../english/doc/manuals.defs:188
msgid "Debian package"
msgstr "Debian 套件"

#: ../../english/doc/manuals.defs:193 ../../english/doc/manuals.defs:197
msgid "Debian package (archived)"
msgstr "Debian 套件（已归档）"

#: ../../english/releases/arches.data:38
msgid "HTML"
msgstr "HTML"

#: ../../english/releases/arches.data:39
msgid "PDF"
msgstr "PDF"

#~ msgid ""
#~ "<p>You can visit the current <a href=\"manuals/<get-var doc />/"
#~ "\">development version</a> here, use <a href=\"cvs\">CVS</a> to download "
#~ "the SGML source text."
#~ msgstr ""
#~ "<p>You can visit the current<a href=\"manuals/<get-var doc />/"
#~ "\">development version</a> here,use <a href=\"cvs\">CVS</a> to download "
#~ "the SGML source text."

#~ msgid ""
#~ "CVS sources working copy: <p>Login to the CVS server using the command:"
#~ "<br>\n"
#~ "<pre>cvs -d:pserver:anonymous@cvs.debian.org:/cvs/debian-policy login</"
#~ "pre>\n"
#~ "and just press enter when you are asked for a password. To check out the "
#~ "sources, use the command:<br>\n"
#~ "<pre>cvs -d:pserver:anonymous@cvs.debian.org:/cvs/debian-policy co <get-"
#~ "var doc /></pre>"
#~ msgstr ""
#~ "CVS sources working copy:<p>Login to the CVS server using the command:"
#~ "<br><pre>cvs -d:pserver:anonymous@cvs.debian.org:/cvs/debian-policy "
#~ "login</pre>and just press enter when you are asked for a password.To "
#~ "check out the sources, use the command:<br><pre>cvs -d:pserver:"
#~ "anonymous@cvs.debian.org:/cvs/debian-policy co <get-var doc /></pre>"

#~ msgid ""
#~ "CVS sources working copy: set <code>CVSROOT</code>\n"
#~ "  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
#~ "  and check out the <kbd>boot-floppies/documentation</kbd> module."
#~ msgstr ""
#~ "取出 CVS 中的原始檔： 將 <code>CVSROOT</code> 設定為\n"
#~ "  <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
#~ "  然後 check out <kbd>boot-floppies/documentation</kbd> module。"

#~ msgid "CVS via web"
#~ msgstr "網頁檢視 CVS"

#~ msgid "Language:"
#~ msgstr "語言:"

#~ msgid ""
#~ "The latest <get-var srctype /> source is available through the <a "
#~ "href=\"https://packages.debian.org/cvs\">Cvs</a> repository."
#~ msgstr ""
#~ "最新的 <get-var srctype /> 格式的源代码可由 <a href=\"https://packages."
#~ "debian.org/cvs\">Cvs</a> 仓库获得。"

#~ msgid ""
#~ "The latest <get-var srctype /> source is available through the <a "
#~ "href=\"https://packages.debian.org/subversion\">Subversion</a> repository."
#~ msgstr ""
#~ "最新的 <get-var srctype /> 格式的源代码可由 <a href=\"https://packages."
#~ "debian.org/subversion\">Subversion</a> 仓库获得。"

#~ msgid ""
#~ "The latest <get-var srctype /> source is available through the <a "
#~ "href=\"https://www.debian.org/doc/cvs\">Subversion</a> repository."
#~ msgstr ""
#~ "<get-var srctype /> 的最新源代码可由 <a href=\"https://www.debian.org/doc/"
#~ "cvs\">Subversion</a> 仓库获得。"

#~ msgid ""
#~ "Use <a href=\"cvs\">SVN</a> to download the SGML source text for <get-var "
#~ "ddp_pkg_loc />."
#~ msgstr ""
#~ "使用 <a href=\"cvs\">SVN</a> 來取得 <get-vard dp_pkg_loc /> 的 SGML 原始"
#~ "檔。"
