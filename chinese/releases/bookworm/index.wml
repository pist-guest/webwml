#use wml::debian::template title="Debian&ldquo;bookworm&rdquo;发行信息"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="77c9da4860132027cb6546223ff0e9b84071a5b7"

<p>Debian <current_release_bookworm> 已\
于 <a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a> 发布。
<ifneq "12.0" "<current_release>"
  "Debian 12.0 最初发布于 <:=spokendate('2023-06-10'):>。"
/>
此次发行包含了许多重要的\
变更，在\
我们的<a href="$(HOME)/News/2023/20230610">新闻稿</a>与\
<a href="releasenotes">发行说明</a>有详细的介绍。</p>

# <p><strong>Debian 12 has been superseded by
# <a href="../trixie/">Debian 13 (<q>trixie</q>)</a>.
# </strong></p>

### This paragraph is orientative, please review before publishing!
# <p><strong>However, bookworm benefits from Long Term Support (LTS) until
# 30th June, 2028. The LTS is limited to i386, amd64, armel, armhf and arm64.
# All other architectures are no longer supported in bookworm.
# For more information, please refer to the <a
# href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
# </strong></p>

<p>
Debian 12 的生命周期为五年：首先是三年的完整 Debian 支持，
至 <:=spokendate('2026-06-10'):>，然后是两年的长期支持（LTS），
至 <:=spokendate('2028-06-30'):>。在 LTS 支持周期中，
受支持的架构会减少。要了解更多信息，
请阅读<a href="$(HOME)/security/">安全信息</a>页面
和 <a  href="https://wiki.debian.org/LTS">Debian Wiki 的 LTS 介绍页面</a>。
</p>

<p>要取得与安装 Debian，请见\
<a href="debian-installer/">安装信息</a>页面与\
<a href="installmanual">安装手册</a>。要从较旧的 Debian 发行版\
升级，请见\
<a href="releasenotes">发行说明</a>的操作指引。</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>以下是 bookworm 最初发布时支持的计算机架构：</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>事与愿违，发行版即使被公开宣布是\
<em>稳定的（stable）</em>，仍可能会存在一些问题。我们已制作了\
<a href="errata">重要已知问题列表</a>，您也可以随时\
<a href="../reportingbugs">回报其他问题</a>给我们。</p>

<p>最后值得一提的是，我们有个<a href="credits">鸣谢</a>\
列表，列出为此次发行版做出贡献的人。</p>
